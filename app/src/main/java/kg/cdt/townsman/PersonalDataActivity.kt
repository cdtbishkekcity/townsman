package kg.cdt.townsman

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.SnackbarUtils
import kg.cdt.townsman.shared_prefs.User
import kg.cdt.townsman.utils.*
import kg.cdt.townsman.utils.ThreadGuiUtils.doInThreadPool
import kg.cdt.townsman.wsapi.WsRestConnector
import kg.cdt.townsman.wsapi.exception.ExceptionDetector
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.login_in_session_is_empty
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.login_or_password_is_not_correct
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.unauthorized
import kg.cdt.townsman.wsapi.exception.TmWsApiRuntimeException
import kg.cdt.townsman.wsapi.model.TownsmanUsers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_personal_data.*

class PersonalDataActivity : AppCompatActivity() {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_data)

        try {
            val getUserInfo = StorageUtils().getUserData().toObject<User>()
            pin_profile.setText(getUserInfo?.user_info?.pin ?: "")
            second_name_profile.setText(getUserInfo?.user_info?.second_name ?: "")
            name_profile.setText(getUserInfo?.user_info?.name ?: "")
            last_name_profile.setText(getUserInfo?.user_info?.last_name ?: "")
            phone_profile.setText(getUserInfo?.login ?: "")
            email_profile.setText(getUserInfo?.user_info?.email ?: "")

            os_version.text = AppAndroidVersionHelper.getAppVersion().replace("""(^[\d.]+)\.(\d+)$""".toRegex(), "v$1 ($2)")

            setSupportActionBar(toolbar_profile)

            save_button.setOnClickListener {
                val pinValue = pin_profile.text.toString().trim()
                val secondNameValue = second_name_profile.text.toString().trim()
                val nameValue = name_profile.text.toString().trim()
                val lastNameValue = last_name_profile.text.toString().trim()
                val phoneValue = phone_profile.text.toString().trim()
                val emailValue = email_profile.text.toString().trim()

                if (pinValue != getUserInfo?.user_info?.pin || secondNameValue != getUserInfo.user_info?.second_name || nameValue != getUserInfo.user_info?.name ||
                    lastNameValue != getUserInfo.user_info?.last_name || phoneValue != getUserInfo.login || emailValue != getUserInfo.user_info?.email
                ) {

                    if (!isPinValid((pinValue))) {
                        ViewHelper().showErrorWithRequestFocus(pin_profile, getString(R.string.pin_not_match_length))
                    } else {
                        // progress dialog init
                        val progressDialog = PrettyProgressDialogUtils().show(
                            this@PersonalDataActivity,
                            getString(R.string.pending_request_msg)
                        )

                        doInThreadPool {
                            try {
                                val setUserPersonal = WsRestConnector.getInstance().setUserInfo(
                                    pinValue,
                                    secondNameValue,
                                    nameValue,
                                    lastNameValue,
                                    emailValue
                                ) ?: TownsmanUsers()

                                runOnUiThread {
                                    progressDialog?.dismiss()
                                }

                                if (setUserPersonal.login.isNotEmpty()) {
                                    LogUtils.info(TAG, "" + setUserPersonal.toString())

                                    // save guardian data 2 storage
                                    val user2storage = User()
                                    user2storage.user_info = setUserPersonal
                                    user2storage.login = phoneValue
                                    user2storage.password = getUserInfo?.password.toString()
                                    StorageUtils().saveUserData(gsonUtils.toJson(user2storage))

                                    runOnUiThread {
                                        ToastUtils.ToastLong(this, getString(R.string.updated_successfully))

                                        exit_profile_button.performClick()
                                    }
                                }
                            } catch (e: TmWsApiRuntimeException) {
                                val exMsg = e.message ?: ""

                                LogUtils.info(TAG, exMsg)

                                val warningMessage =
                                    when (exMsg) {
                                        login_or_password_is_not_correct, unauthorized -> getString(
                                            R.string.logout_auth_token_failed_msg
                                        )
                                        login_in_session_is_empty -> getString(R.string.necessary_login_in_again_msg)
                                        else -> "${getString(R.string.pending_process_failed_msg)}: $exMsg"
                                    }

                                runOnUiThread {
                                    progressDialog?.dismiss()

                                    // в зависимости от ответа сервера показываем сообщение в тоаст или в pretty dialog
                                    when (exMsg) {
                                        login_or_password_is_not_correct, unauthorized, login_in_session_is_empty -> {
                                            ToastUtils.ToastLong(this@PersonalDataActivity, warningMessage)
                                        }
                                        else ->// warning dialog
                                            PrettyDialogUtils().showInfo(
                                                ctx = this@PersonalDataActivity,
                                                title = warningMessage,
                                                message = "",
                                                clickHandler = Runnable {
                                                }
                                            )
                                    }
                                }
                            } catch (e: Exception) {
                                LogUtils.error(TAG, e.message, e)

                                val exMsg = e.message ?: ""

                                val warningMessage =
                                    ExceptionDetector.exceptionDetectorRuntimeException(exMsg, this)

                                runOnUiThread {
                                    progressDialog?.dismiss()

                                    PrettyDialogUtils().showError(
                                        ctx = this@PersonalDataActivity,
                                        title = "${getString(R.string.pending_process_failed_msg)}:",
                                        message = warningMessage
                                    )
                                }
                            }
                        }
                    }
                } else {
                    ToastUtils.ToastLong(
                        this@PersonalDataActivity,
                        getString(R.string.no_changes_msg)
                    )
                }
            }

            exit_profile_button.setOnClickListener {
                finish()
                startActivity(Intent(this, MainActivity::class.java))
            }

            exit_profile.setOnClickListener {
                exit_profile_button.performClick()
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    private fun isPinValid(pinCode: String): Boolean {
        return pinCode.length >= 14
    }

    override fun onBackPressed() {
        super.onBackPressed()

        exit_profile_button.performClick()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_dots_profile, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.change_phone_number -> {
                DialogChangePhoneNumber().show(supportFragmentManager, "custom")
            }
            R.id.exit_profile -> {
                PrettyDialogUtils().showInfo(
                    this,
                    title = ""/*getString(R.string.exit_profile)*/,
                    message = getString(R.string.really_exit_profile),
                    buttonTitle = getString(R.string.exit_profile),
                    cancelable = false,
                    cancelButton = true,
                    clickHandler = Runnable {
                        // progress dialog init
                        val progressDialog = PrettyProgressDialogUtils().show(
                            this@PersonalDataActivity,
                            getString(R.string.pending_request_msg)
                        )

                        doInThreadPool {
                            try {
                                val tryLogout = WsRestConnector.getInstance().tryLogout()

                                runOnUiThread {
                                    progressDialog?.dismiss()
                                }

                                if (tryLogout) {
                                    LogUtils.info(TAG, "tryLogout() success")

                                    finishOnLogOutAndBack2Login()
                                    SnackbarUtils.dismiss()
                                }
                            } catch (e: TmWsApiRuntimeException) {
                                val exMsg = e.message ?: ""

                                LogUtils.info(TAG, exMsg)

                                val warningMessage =
                                    when (exMsg) {
                                        login_or_password_is_not_correct, unauthorized -> getString(R.string.logout_auth_token_failed_msg)
                                        else -> "${getString(R.string.pending_process_failed_msg)}: $exMsg"
                                    }

                                runOnUiThread {
                                    progressDialog?.dismiss()

                                    // в зависимости от ответа сервера показываем сообщение в тоаст или в pretty dialog
                                    when (exMsg) {
                                        login_or_password_is_not_correct, unauthorized -> {
                                            ToastUtils.ToastLong(this@PersonalDataActivity, warningMessage)

                                            finishOnLogOutAndBack2Login()
                                        }
                                        else ->// warning dialog
                                            PrettyDialogUtils().showError(
                                                ctx = this@PersonalDataActivity,
                                                title = warningMessage,
                                                message = "",
                                                clickHandler = Runnable {
                                                    finishOnLogOutAndBack2Login()
                                                }
                                            )
                                    }
                                }
                            } catch (e: Exception) {
                                LogUtils.error(TAG, e.message, e)

                                val exMsg = e.message ?: ""

                                val warningMessage =
                                    ExceptionDetector.exceptionDetectorRuntimeException(exMsg, this)

                                runOnUiThread {
                                    progressDialog?.dismiss()

                                    PrettyDialogUtils().showError(
                                        ctx = this@PersonalDataActivity,
                                        title = "${getString(R.string.pending_process_failed_msg)}:",
                                        message = warningMessage,
                                        clickHandler = Runnable {
                                            finishOnLogOutAndBack2Login()
                                        }
                                    )
                                }
                            }
                        }
                    }, setGravity = Gravity.BOTTOM
                )
            }
        }
        return true
    }

    private fun finishOnLogOutAndBack2Login() {
        StorageUtils().removeSessionHeader()
        StorageUtils().removeUserData()
        StorageUtils().removeCurrentActivity()
        StorageUtils().removeCurrentRole()
        StorageUtils().removePushToken()
        StorageUtils().removeCurrentActivity()
        finish()
        startActivity(Intent(this, LoginActivity::class.java))
    }

}