package kg.cdt.townsman

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.EncryptUtils
import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.authentication_failed
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.login_or_password_is_not_correct
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.user_is_disabled
import kg.cdt.townsman.common_permissions.MultiPermissionsRequest
import kg.cdt.townsman.security.HashSettings
import kg.cdt.townsman.utils.*
import kg.cdt.townsman.utils.ThreadGuiUtils.doInThreadPool
import kg.cdt.townsman.wsapi.WsRestConnector
import kg.cdt.townsman.wsapi.exception.ExceptionDetector
import kg.cdt.townsman.wsapi.exception.TmWsApiRuntimeException
import kg.cdt.townsman.wsapi.model.TownsmanUsers
import kg.cdt.townsman.wsapi.model.UserLanguages
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    var phoneNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        try {
            // begin init
            beginInit()

            phone.error = null
            sms_code.error = null

            sign_in_button.setOnClickListener {
                val login = phone.text.toString().trim()
                val smsCodeStr = sms_code.text.toString().trim()

                try {
                        when {
                            login.isEmpty() -> {
                                ViewHelper().showErrorWithRequestFocus(phone, getString(R.string.text_obligatory_field))
                            }
                            !isPhoneValid(login) -> {
                                ViewHelper().showErrorWithRequestFocus(phone, getString(R.string.error_invalid_phone))
                            }
                            smsCodeStr.isEmpty() -> {
                                ViewHelper().showErrorWithRequestFocus(sms_code, getString(R.string.text_obligatory_field))
                            }
                            !isCodeSMSValid(smsCodeStr) -> {
                                ViewHelper().showErrorWithRequestFocus(sms_code, getString(R.string.error_invalid_code_sms_length))
                            }
                            !MultiPermissionsRequest(this).permissionsRequest(this) -> {
                                return@setOnClickListener
                            }
                            else -> {
                                // progress dialog init
                                val progressDialog = PrettyProgressDialogUtils().show(
                                    this@LoginActivity,
                                    getString(R.string.pending_request_msg)
                                )

                                doInThreadPool {
                                    try {
                                        // password to hash: hmac-sha384
                                        val passwordHash = EncryptUtils.encryptHmacSHA384ToString(smsCodeStr, HashSettings.getHashSalt())

                                        val tryLogin = WsRestConnector.getInstance().tryLogin(login, passwordHash)

                                        runOnUiThread {
                                            progressDialog?.dismiss()
                                        }

                                        if (tryLogin) {
                                            if (WsRestConnector.getInstance().SessionHeader.isEmpty()) {
                                                throw TmWsApiRuntimeException(getString(R.string.authentication_failed_msg))
                                            }

                                            LogUtils.info(TAG, "tryLogin() success")

                                            val authorization = TownsmanUsers()
                                            authorization.login = login
                                            authorization.sms_code = passwordHash

                                            // save rest session header 2 storage
                                            StorageUtils().saveSessionHeader(WsRestConnector.getInstance().SessionHeader)
                                            StorageUtils().saveUserData(gsonUtils.toJson(authorization))

                                            LoginHelper().saveLoginStates2storage(phone)

                                            finish()
                                            startActivity(Intent(this, MainActivity::class.java))
                                        }
                                    } catch (e: TmWsApiRuntimeException) {
                                        val exMsg = e.message ?: ""

                                        LogUtils.info(TAG, exMsg)

                                        val warningMessage =
                                            when (exMsg) {
                                                login_or_password_is_not_correct, authentication_failed -> getString(
                                                    R.string.authentication_login_password_failed_msg
                                                )
                                                TmWsApiMessages.unauthorized -> getString(
                                                    R.string.logout_auth_token_failed_msg
                                                )
                                                TmWsApiMessages.login_in_session_is_empty -> getString(
                                                    R.string.necessary_login_in_again_msg
                                                )
                                                else -> "${getString(R.string.pending_process_failed_msg)}: $exMsg"
                                            }

                                        runOnUiThread {
                                            progressDialog?.dismiss()

                                            // в зависимости от ответа сервера показываем сообщение в текстовом поле или в pretty dialog
                                            when (exMsg) {
                                                login_or_password_is_not_correct -> {
                                                    sms_code.text?.clear()
                                                    sms_code.error = warningMessage
                                                    sms_code.requestFocus()
                                                }
                                                authentication_failed, user_is_disabled -> {
                                                    phone.error = warningMessage
                                                    phone.requestFocus()
                                                }
                                                else ->// warning dialog
                                                    PrettyDialogUtils().showError(
                                                        ctx = this@LoginActivity,
                                                        title = warningMessage,
                                                        message = "",
                                                        cancelable = true
                                                    )
                                            }
                                        }
                                    } catch (e: Exception) {
                                        LogUtils.error(TAG, e.message, e)

                                        val exMsg = e.message ?: ""

                                        val warningMessage = ExceptionDetector.exceptionDetectorRuntimeException(exMsg, this)

                                        runOnUiThread {
                                            progressDialog?.dismiss()

                                            PrettyDialogUtils().showError(
                                                ctx = this@LoginActivity,
                                                title = "${getString(R.string.authentication_failed_msg)}:",
                                                message = warningMessage
                                            )
                                        }
                                    }
                                }
                            }
                        }
                } catch (e: Exception) {
                    LogUtils.error(TAG, e.message, e)
                }
            }

            phone.setOnFocusChangeListener { _, hasFocus ->
                try {
                    val login = phone.text.toString().trim()

                    if (!hasFocus && phoneNumber != login) {
                        val regexString = ViewHelper().regexPhoneNumber(phone, login)

                        phoneNumber = regexString

                        if (!isPhoneValid(login)) {
                            phone.error = getString(R.string.error_invalid_phone)
                        } else {
                            ToastUtils.ToastShort(this, "toast has focus")

                            sendRequestsSms(regexString)
                        }
                    }
                } catch (e: Exception) {
                    LogUtils.error(TAG, e.message, e)
                }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }

    private fun beginInit() {
        try {// storage prefs init
            StorageUtils().init(this, AppUtils.getAppPackageName())
            // load current activity
            loadCurrentActivity()
            // language load
            if (!Prefs.contains(StorageUtils.LANGUAGE)) {
                LanguageUtils(activity = this).setLocate(UserLanguages.RU.toString(), this)
                LanguageUtils(activity = this).show(supportFragmentManager, ContentValues.TAG)
            } else if (Prefs.contains(StorageUtils.LANGUAGE) && Prefs.getString(StorageUtils.LANGUAGE, "").isEmpty()) {
                LanguageUtils(activity = this).setLocate(UserLanguages.RU.toString(), this)
                LanguageUtils(activity = this).show(supportFragmentManager, ContentValues.TAG)
            } else {
                LanguageUtils(activity = this).loadLocate(this)
            }
            //add mask phone
            PhoneNumberHelper().phoneMask(phone, PhoneNumberHelper().phoneMaskFormat2)
            //show 996
            phone.setText("")
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }

    private fun loadCurrentActivity() {
        try {
            when (StorageUtils().getCurrentActivity()) {
                "MainActivity" -> {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                else -> {
                    // load list of saved login
                    LoginHelper().loadListLogin(this, phone, sms_code)
                }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    fun isPhoneValid(phoneNumber: String): Boolean {
        return phoneNumber.length >= 12
    }

    private fun isCodeSMSValid(pinCode: String): Boolean {
        return pinCode.length >= 6
    }

    private fun sendRequestsSms(phoneNumber: String) {
/*        doInThreadPool {
            try {
                Thread.sleep(1000L)

                val sendPatientRequestsSms = WsRestConnector.getInstance().sendPatientRequestsSms(
                    phoneNumber,
                    getString(R.string.authorization_code)
                )

                if (sendPatientRequestsSms == true) {
                    runOnUiThread {
                        ToastUtils.ToastLong(this, getString(R.string.sms_sent_successfully))
                    }
                }

            } catch (e: TmWsApiRuntimeException) {
                val exMsg = e.message ?: ""

                LogUtils.info(TAG, exMsg)

                val warningMessage =
                    when (exMsg) {
                        user_does_not_exists -> getString(R.string.number_was_not_found_register)
                        operator_not_added -> getString(R.string.сheck_correct_dialed_number)
                        too_many_requests -> getString(R.string.too_many_requests)

                        else -> "${getString(R.string.authentication_failed_msg)}: $exMsg"
                    }

                runOnUiThread {
                    //                    progressDialog?.dismiss()

                    // в зависимости от ответа сервера показываем сообщение в текстовом поле или в pretty dialog
                    when (exMsg) {
                        else ->// warning dialog
                            PrettyDialogUtils().showInfo(
                                ctx = this@LoginActivity,
                                title = warningMessage,
                                message = "",
                                cancelable = true
                            )
                    }
                }
            } catch (e: Exception) {
                LogUtils.error(TAG, e.message, e)

                val exMsg = e.message ?: ""

                val warningMessage =
                    ExceptionDetector.exceptionDetectorRuntimeException(exMsg, this)

                runOnUiThread {
//                    progressDialog?.dismiss()

                    PrettyDialogUtils().showError(
                        ctx = this@LoginActivity,
                        title = "${getString(R.string.pending_process_failed_msg)}:",
                        message = warningMessage
                    )
                }
            }
        }*/
    }

    override fun onBackPressed() {
        super.onBackPressed()

        finish()
    }
}

