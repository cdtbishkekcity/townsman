package kg.cdt.townsman

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.AppUtils
import kg.cdt.townsman.common_permissions.MultiPermissionsRequest
import kg.cdt.townsman.firebase_services.model.FireBaseRequest
import kg.cdt.townsman.shared_prefs.User
import kg.cdt.townsman.utils.*
import kg.cdt.townsman.utils.ThreadGuiUtils.doInThreadPool
import kg.cdt.townsman.wsapi.WsRestConnector
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.login_in_session_is_empty
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.login_or_password_is_not_correct
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.unauthorized
import kg.cdt.townsman.wsapi.exception.TmWsApiRuntimeException
import kg.cdt.townsman.wsapi.model.BaseResponse
import kg.cdt.townsman.wsapi.model.TownsmanUsers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //begin init
        beginInit()

        appeal_tile.setOnClickListener(this)
        map_org_tile.setOnClickListener(this)
        service_tile.setOnClickListener(this)
        settings_tile.setOnClickListener(this)
        site_tile.setOnClickListener(this)
        profile_tile.setOnClickListener {
            finish()
            startActivity(Intent(this, PersonalDataActivity::class.java))
        }

        // dialog update app
        if (intent != null) {
            if (intent.getBooleanExtra(FireBaseRequest.Data.FsPushCmdEnum.app_new_ver_available.toString(), false)) {
                try {
                    PrettyDialogUtils().showInfo(
                        ctx = this@MainActivity,
                        title = "",
                        message = getString(R.string.please_press_ok_and_google_play_market),
                        clickHandler = Runnable {
                            val appPackageName = packageName // getPackageName() from Context or Activity object
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                            } catch (anfe: ActivityNotFoundException) {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                            }
                        }
                    )
                } catch (e: Exception) {
                    LogUtils.error(TAG, e.message)
                }
            }
        }
    }

    private fun beginInit() {
        try {
            // storage prefs init
            StorageUtils().init(this, AppUtils.getAppPackageName())
            // set current activity
            StorageUtils().saveCurrentActivity(this.localClassName)
            // lang load
            LanguageUtils(activity = this).loadLocate(this)
            // rest session get from storage
            WsRestConnector.getInstance().SessionHeader = StorageUtils().getSessionHeader()
            val supervisorStorage = StorageUtils().getUserData().toObject<User>()
            WsRestConnector.getInstance().Login = supervisorStorage?.login.toString()
            WsRestConnector.getInstance().Password = supervisorStorage?.password.toString()
            // re save session if auto login
            WsRestConnector.autoLoginOnUnauthorized = object : RunnableUtils {
                override fun run(obj: Any?) {
                }

                override fun run() {
                    StorageUtils().saveSessionHeader(WsRestConnector.getInstance().SessionHeader)
                }
            }
            connectingProgressVisible(View.VISIBLE)
            // try get current user's info from rest

            doInThreadPool {
                try {
                    Thread.sleep(300L)

                    val getUserPersonal = WsRestConnector.getInstance().getUserPersonal() ?: BaseResponse.Result()
                    val getUserInfo = getUserPersonal.user_info
                    if (getUserInfo?.login?.isNotEmpty() == true) {
                        LogUtils.info(TAG, getUserInfo.toString())

                        val supervisorData = StorageUtils().getUserData().toObject<User>()
                        supervisorData?.user_info = getUserInfo
                        StorageUtils().saveUserData(gsonUtils.toJson(supervisorData ?: User()))

//                        StorageUtils().saveUserAppSettingsIfDiff(getUserPersonal.mobile_app_settings)

                        // push token send
                        val pushToken = StorageUtils().getPushToken()
                        val appVersion = AppAndroidVersionHelper.getAppVersion()
                        val osVersion = AppAndroidVersionHelper.getOsVersion()
                        if (pushToken.isNotEmpty() && getUserInfo.registered_push_token != pushToken ||
                            (appVersion != getUserInfo.app_version || osVersion != getUserInfo.os_version)) {
                            val setPushToken = WsRestConnector.getInstance().setRegisteredPushToken(
                                pushToken,
                                appVersion,
                                osVersion
                            ) ?: TownsmanUsers()
                            if (setPushToken.login.isNotEmpty()) {
                                LogUtils.info(TAG, "push token, app-os version sent: $setPushToken")

                                supervisorData?.user_info = setPushToken
                            }
                        }
                    }

                    connectingProgressVisible(View.GONE)
                } catch (e: TmWsApiRuntimeException) {
                    val exMsg = e.message ?: ""

                    LogUtils.info(TAG, exMsg)

                    val warningMessage =
                        when (exMsg) {
                            login_or_password_is_not_correct, unauthorized -> getString(R.string.logout_auth_token_failed_msg)
                            login_in_session_is_empty -> getString(R.string.necessary_login_in_again_msg)
                            else -> "${getString(R.string.pending_process_failed_msg)}: $exMsg"
                        }

                    runOnUiThread {
                        // в зависимости от ответа сервера показываем сообщение в тоаст или в pretty dialog
                        when (exMsg) {
                            login_or_password_is_not_correct, unauthorized, login_in_session_is_empty -> {
                                ToastUtils.ToastLong(this@MainActivity, warningMessage)

                                finishOnLogOutAndBack2Login()
                            }
                            else ->// warning dialog
                                PrettyDialogUtils().showError(
                                    ctx = this@MainActivity,
                                    title = warningMessage,
                                    message = "",
                                    clickHandler = Runnable { }
                                )
                        }
                    }
                } catch (e: Exception) {
                    LogUtils.error(TAG, e.message, e)
                }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.appeal_tile -> {
                startActivity(Intent(this, AppealActivity::class.java))
            }
            R.id.map_org_tile -> {
                if (!MultiPermissionsRequest(this).checkPermissionGeoLocation(this, this)) {
                    return
                } else {
                    startActivity(Intent(this, MapsActivity::class.java))
                }
            }
            R.id.service_tile -> {
            }
            R.id.settings_tile -> {
            }
            R.id.site_tile -> {
            }
        }
    }

    private fun connectingProgressVisible(visibilityCode: Int) {
        runOnUiThread {
            connection_progress.visibility = visibilityCode
        }
    }

    private fun finishOnLogOutAndBack2Login() {
        StorageUtils().removeSessionHeader()
        StorageUtils().removeUserData()
        StorageUtils().removeCurrentActivity()
        StorageUtils().removeCurrentRole()
        StorageUtils().saveCurrentActivity("")
        finish()
        startActivity(Intent(this, LoginActivity::class.java))

    }
}