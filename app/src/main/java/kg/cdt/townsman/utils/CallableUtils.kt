package kg.cdt.townsman.utils

import java.util.concurrent.Callable

interface CallableUtils : Callable<Any> {
    fun call(obj: Any): Any?
}
