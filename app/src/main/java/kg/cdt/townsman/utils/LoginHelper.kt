package kg.cdt.townsman.utils

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.R
import kg.cdt.townsman.utils.ThreadGuiUtils.doInPostDelayedHandler
import libs.mjn.prettydialog.PrettyDialog

class LoginHelper {
    private val TAG = this.javaClass.name

    fun loadListLogin(context: Context, phone: TextView, pinCode: TextView) {
        val loginStates = StorageUtils().getLoginState()
        try {
            // load list of saved logins
            val listString = loginStates
                .split(",")
                .filter { it.isNotEmpty() }
                .toTypedArray()

            if (listString.isEmpty()) {
                phone.requestFocus()
                keyBoardRequest(phone, context)

            } else if (listString.isNotEmpty()) {
                val dialogRun = Runnable {
                    val pDialog = PrettyDialog(context)
                    pDialog
                        .setTitle(context.getString(R.string.choice_login))
                        .setTitleColor(R.color.pdlg_color_blue)
                        .setIcon(R.drawable.pdlg_icon_success)
                        .setIconTint(R.color.pdlg_color_blue)

                    // generate the names as buttons
                    for (nameLogin in listString) {
                        pDialog.addButton(
                            nameLogin,
                            R.color.pdlg_color_blue,
                            android.R.color.white
                        ) {
                            phone.text = nameLogin

                            doInPostDelayedHandler({
                                pinCode.requestFocus()
                                keyBoardRequest(pinCode, context)

                            }, 100L)

                            pDialog.dismiss()
                        }
                    }

                    //add button: input self name
                    pDialog
                        .addButton(
                            context.getString(R.string.input_me),
                            R.color.pdlg_color_black,
                            R.color.pdlg_color_white
                        ) {
                            doInPostDelayedHandler({
                                phone.requestFocus()
                                keyBoardRequest(phone, context)
                            }, 100L)

                            pDialog.dismiss()
                        }
                        .setGravity(Gravity.BOTTOM)
                        .show()
                }

                phone.setOnClickListener {
                    keyBoardRequest(phone, context)
                    loadListLogin(context, phone, pinCode)
                }

                doInPostDelayedHandler({
                    dialogRun.run()
                }, 100L)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    private fun keyBoardRequest(view: View, context: Context) {
        try {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    // Список при авторизации выбор пользователя
    fun saveLoginStates2storage(phone: TextView) {
        val login2lower = phone.text.toString()

        // save login list state for fast choice
        val split2Array = StorageUtils().getLoginState()
            .split(",")
            .toTypedArray()
            .filter { it.isNotEmpty() }
            .toTypedArray()

        if (!split2Array.contains(login2lower)) {
            // save login 2 list in storage
            if (split2Array.isEmpty()) {
                StorageUtils().saveLoginState(login2lower)
            } else {
                Prefs.putString(
                    StorageUtils.LOGIN_STATE,
                    split2Array.plus(login2lower).joinToString(",")
                )
            }
        }
    }
}