package kg.cdt.townsman.utils

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator

class VibrationHelper {
    private val TAG = this.javaClass.name

    fun doVibration(context: Context, time: Long = 1000L) {
        try {
            if (Build.VERSION.SDK_INT >= 26) {
                (context.getSystemService(VIBRATOR_SERVICE) as Vibrator).vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                (context.getSystemService(VIBRATOR_SERVICE) as Vibrator).vibrate(time)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }
}