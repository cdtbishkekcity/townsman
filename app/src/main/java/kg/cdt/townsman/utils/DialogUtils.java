package kg.cdt.townsman.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;

import java.util.ArrayList;

import kg.cdt.townsman.R;


public class DialogUtils {

    public static void showDialog(Activity activity, final String title, final String message, DialogInterface.OnClickListener onClickListener) {
        try {
            new AlertDialog
                    .Builder(activity)
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(R.string.OK, onClickListener).show();
        } catch (Exception e) {
            Log.e(activity.getString(R.string.app_name), "Error occurred while showing dialog.", e);
        }
    }

    public static void showDialog(final Activity activity, final String title, final String message) {
        showDialog(activity, title, message, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d(activity.getString(R.string.app_name), "UserRegistrationTasks closed the dialog");
            }
        });
    }

    public static void showConfirmation(Context context, final String title, final String message, final DialogInterface.OnClickListener onPositiveButtonClickListener, final DialogInterface.OnClickListener onNegativeButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton(context.getResources().getString(R.string.YES),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (onPositiveButtonClickListener != null) {
                                    onPositiveButtonClickListener.onClick(dialog, id);
                                }
                            }
                        })
                .setNegativeButton(context.getResources().getString(R.string.NO),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (onNegativeButtonClickListener != null) {
                                    onNegativeButtonClickListener.onClick(dialog, id);
                                }
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();
    }

    public static void showAlertDialogWithItems(String title, ArrayList<String> listString, Activity activity, DialogInterface.OnClickListener onClickListener) {
        showAlertDialogWithItems(title, listString.toArray(new String[0]), activity, onClickListener);
    }

    public static void showAlertDialogWithItems(String title, String[] items, Activity activity, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        builder.setItems(items, onClickListener);
        AlertDialog dialog = builder.create();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    /*public static AlertDialog showDialogYesNo(Activity context, final Object message, final View.OnClickListener onPositiveButtonClickListener, final View.OnClickListener onNegativeButtonClickListener) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.details_dialog, null);

        dialogBuilder.setView(dialogView);
        TextView mMessage = (TextView) dialogView.findViewById(R.id.dtMessage);
        TextView mOk = (TextView) dialogView.findViewById(R.id.dtYes);
        TextView mNo = (TextView) dialogView.findViewById(R.id.dtNo);

        if (message instanceof String)
            mMessage.setText((String) message);
        else if (message instanceof Spanned)
            mMessage.setText((Spanned) message);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (onPositiveButtonClickListener != null)
                    onPositiveButtonClickListener.onClick(v);
            }
        });
        mNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (onNegativeButtonClickListener != null)
                    onNegativeButtonClickListener.onClick(v);
            }
        });

        return alertDialog;
    }*/

    public static void showErrorMessage(final Activity activity, final String message, final boolean closeActivity, final boolean closeApplication) {
        showDialog(activity, activity.getString(R.string.app_name), message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (closeActivity) {
                    activity.finish();
                }
                if (closeApplication) {
                    activity.moveTaskToBack(true);
                }
            }
        });
    }

    public static void showTempDialogWithDelay(final String text, Activity activity) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(activity).setMessage(text);
        final AlertDialog alert = dialog.create();
        alert.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        alert.show();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (alert.isShowing()) {
                    alert.dismiss();
                }
            }
        };

        final Handler handler = new Handler();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });
        handler.postDelayed(runnable, 1500);
    }

    public static void showDialogInPostHandler(final Activity activity, final String title, final String message) {
        ThreadGuiUtils.doInPostHandler(new Runnable() {
            @Override
            public void run() {
                showDialog(activity, title, message);
            }
        });
    }

    public static void showDialogInPostHandler(final Activity activity, final String title, final String message, final DialogInterface.OnClickListener onClickListener) {
        ThreadGuiUtils.doInPostHandler(new Runnable() {
            @Override
            public void run() {
                showDialog(activity, title, message, onClickListener);
            }
        });
    }
}