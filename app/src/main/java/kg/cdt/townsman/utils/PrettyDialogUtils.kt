package kg.cdt.townsman.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.Window
import android.widget.TextView
import kg.cdt.townsman.R
import libs.mjn.prettydialog.PrettyDialog

class PrettyDialogUtils {
    private val TAG = this.javaClass.name

    fun showError(
        ctx: Context,
        title: String,
        message: String,
        buttonTitle: String = String(),
        cancelable: Boolean = false,
        clickHandler: Runnable? = null,
        cancelButton: Boolean = false,
        setGravity: Int = Gravity.CENTER
    ): PrettyDialog? {
        try {
            var btnTitle = ctx.getString(R.string.OK)
            if (buttonTitle.isNotEmpty()) {
                btnTitle = buttonTitle
            }

            val pDialog = PrettyDialog(ctx)
            pDialog
                .setTitle(title)
                .setTitleColor(R.color.pdlg_color_red)
                .setMessage(message)
                .setMessageColor(R.color.pdlg_color_black)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_red)
                .addButton(
                    btnTitle,
                    R.color.pdlg_color_black,
                    android.R.color.white
                ) {
                    clickHandler?.run()

                    pDialog.dismiss()
                }
                .setGravity(setGravity)
                .setCancelable(cancelable)

            if (cancelButton) {
                pDialog.addButton(
                    ctx.getString(R.string.CANCEL),
                    R.color.pdlg_color_black,
                    android.R.color.white
                ) {
                    pDialog.dismiss()
                }
            }

            pDialog.show()

            return pDialog

        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }

        return null
    }

    fun showInfo(
        ctx: Context,
        title: String,
        message: String,
        buttonTitle: String = String(),
        cancelable: Boolean = false,
        clickHandler: Runnable? = null,
        cancelButton: Boolean = false,
        setGravity: Int = Gravity.CENTER
    ): PrettyDialog? {
        try {
            var btnTitle = ctx.getString(R.string.OK)
            if (buttonTitle.isNotEmpty()) {
                btnTitle = buttonTitle
            }

            val pDialog = PrettyDialog(ctx)
            pDialog
                .setTitle(title)
                .setTitleColor(R.color.pdlg_color_blue)
                .setMessage(message)
                .setMessageColor(R.color.pdlg_color_black)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_blue)
                .addButton(
                    btnTitle,
                    R.color.pdlg_color_black,
                    android.R.color.white
                ) {
                    clickHandler?.run()

                    pDialog.dismiss()
                }
                .setGravity(setGravity)
                .setCancelable(cancelable)

            if (cancelButton) {
                pDialog.addButton(
                    ctx.getString(R.string.CANCEL),
                    R.color.pdlg_color_black,
                    android.R.color.white
                ) {
                    pDialog.dismiss()
                }
            }

            pDialog.show()

            return pDialog

        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return null
    }

    fun showDialogProgress(context: Context, description: String, cancelable: Boolean): Dialog {
        val progressDialog = Dialog(context)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.progress_dialog)
        val textView = progressDialog.findViewById<TextView>(R.id.text_loading) as TextView
        textView.text = description
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(cancelable)
        progressDialog.show()
        return progressDialog
    }
}