package kg.cdt.townsman.utils

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.blankj.utilcode.util.AppUtils
import kg.cdt.townsman.R
import java.util.*

class NotificationHelper {
    private val TAG = this.javaClass.name

    private lateinit var builder: Notification.Builder
    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationChannel: NotificationChannel
    private val channelId = AppUtils.getAppPackageName()

    private val adminChannelId = "admin_channel"

    fun notificationInit(
        context: Context,
        service: Service,
        title: String,
        message: String,
        intent: Intent?,
        icon: Int = R.drawable.ic_notification_icon
    ) {
        try {
            val normalLayout = RemoteViews(context.packageName, R.layout.custom_notification_layout)
            val expandedlLayout = RemoteViews(context.packageName, R.layout.custom_expanded_notification_layout)

            normalLayout.setTextViewText(R.id.notification_info, message)
            normalLayout.setTextViewText(R.id.notification_title, title)
            expandedlLayout.setTextViewText(R.id.notification_info, message)
            expandedlLayout.setTextViewText(R.id.notification_title, title)
            notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val notificationID = Random().nextInt(3000)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel = NotificationChannel(
                    channelId,
                    context.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH
                )

                notificationChannel.enableLights(true)
                notificationChannel.enableVibration(true)
                notificationManager.createNotificationChannel(notificationChannel)

                val pendingIntent = PendingIntent.getActivity(
                    context, 0, intent,
                    PendingIntent.FLAG_CANCEL_CURRENT
                )

                val vibrate = longArrayOf(1000, 1000, 1000, 1000, 1000)
                val notificationSoundUri =
                    RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                builder = Notification.Builder(context, channelId)
//                    .setContentTitle(title)
//                    .setContentText(message)
                    .setCustomContentView(normalLayout)
                    .setContent(normalLayout)
//                    .setCustomBigContentView(expandedlLayout)
                    .setSmallIcon(icon)
                    .setAutoCancel(true)
                    .setSound(notificationSoundUri)
                    .setVibrate(vibrate)
                    .setContentIntent(pendingIntent)
                    service.startForeground(notificationID, builder.build())
            } else {
                val pendingIntent = PendingIntent.getActivity(
                    context, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT
                )

                val builderc = NotificationCompat.Builder(context)
//                    .setContentTitle(title)
//                    .setContentText(format2Html(message))
//                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                    .setStyle(
//                        NotificationCompat.BigTextStyle().bigText(format2Html(message)).setBigContentTitle(
//                            title
//                        )
//                    )
//                    .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                    .setCustomContentView(normalLayout)
                    .setContent(normalLayout)
//                    .setCustomBigContentView(expandedlLayout)
                    .setSmallIcon(R.drawable.person)
                    .setContentIntent(pendingIntent)
                val notification = builderc.build()
                notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    service.startForeground(notificationID, notification)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }

    fun fsNotification(
        context: Context,
        title: String,
        message: String,
        intent: Intent?,
        icon: Int = R.drawable.ic_notification_icon
    ) {
        try {
            val normalLayout = RemoteViews(context.packageName, R.layout.custom_notification_layout)
            val expandedlLayout = RemoteViews(context.packageName, R.layout.custom_expanded_notification_layout)
            normalLayout.setTextViewText(R.id.notification_info, message)
            normalLayout.setTextViewText(R.id.notification_title, title)
            expandedlLayout.setTextViewText(R.id.notification_info, message)
            expandedlLayout.setTextViewText(R.id.notification_title, title)

            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationID = Random().nextInt(3000)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setupChannels(notificationManager)
            }

            intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent?.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            val pendingIntent = PendingIntent.getActivity(
                context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
            )

            val rawBitmap = BitmapFactory.decodeResource(
                context.resources,
                R.mipmap.ic_launcher
            )
            val vibrate = longArrayOf(1000, 1000, 1000, 1000, 1000)
            val notificationSoundUri =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(context, adminChannelId)
                .setSmallIcon(icon)
                .setLargeIcon(rawBitmap)
//                .setContentTitle(title)
//                .setContentText(format2Html(message))
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setStyle(
//                    NotificationCompat.BigTextStyle().bigText(format2Html(message)).setBigContentTitle(
//                        title
//                    )
//                )
//                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(normalLayout)
//                .setCustomBigContentView(expandedlLayout)
                .setContent(normalLayout)
                .setAutoCancel(true)
                .setSound(notificationSoundUri)
                .setVibrate(vibrate)
                .setContentIntent(pendingIntent)
                .setOngoing(true)

            notificationManager.notify(notificationID, notificationBuilder.build())
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager?) {
        try {
            val adminChannelName = AppUtils.getAppPackageName()
            val adminChannelDescription = "service and push notification"

            val adminChannel: NotificationChannel
            adminChannel = NotificationChannel(
                adminChannelId,
                adminChannelName,
                NotificationManager.IMPORTANCE_HIGH
            )
            adminChannel.description = adminChannelDescription
            adminChannel.enableLights(true)
            adminChannel.lightColor = Color.RED
            adminChannel.enableVibration(true)
            notificationManager?.createNotificationChannel(adminChannel)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    private fun format2Html(text: String): SpannableString {
        return SpannableString(
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) Html.fromHtml(text)
            else Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        )
    }
}