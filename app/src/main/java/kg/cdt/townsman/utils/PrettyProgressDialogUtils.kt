package kg.cdt.townsman.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.TextView
import kg.cdt.townsman.R

class PrettyProgressDialogUtils {
    private val TAG = this.javaClass.name

    fun show(
        ctx: Context,
        message: String = "",
        cancelable: Boolean = false
    ): Dialog? {
        try {
            val progressDialog = Dialog(ctx)
            progressDialog.run {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                setContentView(R.layout.progress_dialog)
                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCancelable(cancelable)
                val textView = progressDialog.findViewById(R.id.text_loading) as TextView
                textView.text = message
                show()
            }

            return progressDialog
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return null
    }
}