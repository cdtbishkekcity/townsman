package kg.cdt.townsman.utils

import android.widget.EditText
import android.widget.TextView
import java.util.regex.Pattern

class ViewHelper {
    private val TAG = this.javaClass.name
    fun showError(textView: TextView, error: String) {
        try {
            textView.error = error
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    fun showErrorWithRequestFocus(textView: TextView, error: String) {
        try {
            textView.error = error
            textView.requestFocus()
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    fun regexPhoneNumber(setText: EditText, getText: String): String {
        try {
            val replaceString = getText.replace("""^(\d{3})(0)?(\d{9})(\d+)?""".toRegex(), "$1$3")
            setText.setText(replaceString)
            return replaceString
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return ""
    }

    fun checkSpecCharacters(getText: String): Boolean {
        try {
            val pattern = "[а-яА-ЯёЁa-zA-Z0-9]+"
            if (!Pattern.matches(pattern, getText)) {
                return true
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return false
    }

    fun removeSpacesCharacters(setText: EditText, getText: String) {
        try {
            val replaceString = getText.replace("""\s+""".toRegex(), "")
            setText.setText(replaceString)
        } catch (e: Exception) {

        }
    }

}