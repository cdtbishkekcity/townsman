package kg.cdt.townsman.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup

object Application {
    const val AppName = "Учет производства работ"

    @get:Synchronized
    @set:Synchronized
    private var CancelExitApp = false

    fun killApplication() {
        android.os.Process.killProcess(android.os.Process.myPid())
    }

    @SuppressLint("LongLogTag")
    fun Exit() {
        Log.d(AppName, "application exiting...")

        Runtime.getRuntime().exit(0)
    }

    fun APIVersionSDK(): Int {
        return Build.VERSION.SDK_INT
    }

    fun getAppVersion(context: Context): String {
        try {
            return context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            return if (e.message != null) e.message!! else ""
        }
    }

    fun unbindDrawables(view: View) {
        Log.d("unbindDrawables", "unbindDrawables")

        if (view.background != null) {
            view.background.callback = null
        }
        if (view is ViewGroup) {
            var i = 0
            while (i < view.childCount && i <= 1000) {
                unbindDrawables(view.getChildAt(i))
                i++
            }
            view.removeAllViews()
        }
    }

    fun finishActivityWithDelay(activity: Activity, sleep: Long = 0L) {
        if (sleep > 0) {
            try {
                Thread.sleep(sleep)
            } catch (e: Exception) {
            }
        }

        activity.finish()
    }
}