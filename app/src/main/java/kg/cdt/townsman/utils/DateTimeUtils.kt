package kg.cdt.townsman.utils


import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateTimeUtils {
    val format = "dd-MM-yyyy HH:mm:ss"
    val mySqlFormat = "yyyy-MM-dd HH:mm:ss"

    /**
     * @return Now time in string
     */
    fun now() = SimpleDateFormat(mySqlFormat, Locale.ENGLISH).format(Date())

    /**
     * @return Now time in string with milliseconds
     */
    fun nowMs() = SimpleDateFormat("$mySqlFormat.SSS", Locale.ENGLISH).format(Date()) ?: ""

    /**
     * Compare 2 time
     *
     * @param timeString1
     * @param timeString2
     * @return if <= 0 timeString1 is an earlier date than timeString2
     */
    fun compare2time(
        timeString1: String,
        timeString2: String
    ): Int {
        try {
            val timeFormat = SimpleDateFormat(format)

            val date1 = timeFormat.parse(timeString1)
            val date2 = timeFormat.parse(timeString2)

            return date1.compareTo(date2)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return -1
    }

    /**
     * Get a difference between timeFrom and timeTo
     *
     * @param timeFrom
     * @param timeTo
     * @return difference in seconds
     */
    fun getTimeDiff(
        timeFrom: String,
        timeTo: String
    ): Long {
        try {
            val timeFormat = SimpleDateFormat(format)
            val diff = timeFormat.parse(timeFrom).time - timeFormat.parse(timeTo).time

            return TimeUnit.MILLISECONDS.toSeconds(diff)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return -1L
    }

//    fun numberDaysBetweenDates(context: Context, time: String): String {
//        val myFormat = SimpleDateFormat("yyyy-MM-dd")
//
//
//        val date1 = myFormat.parse(TimeUtils.getNowString())
//        val date2 = myFormat.parse(time)
//        val diff = date2!!.time - date1!!.time
//
//        return when {
//            TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) == 0L -> context.getString(R.string.today_at)
//            TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) == -1L -> context.getString(R.string.yesterday_in)
//            TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) == -2L -> context.getString(R.string.before_yesterday_in)
//            else -> ""
//        }
//    }
}