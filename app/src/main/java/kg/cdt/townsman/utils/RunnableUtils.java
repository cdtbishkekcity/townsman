package kg.cdt.townsman.utils;

public interface RunnableUtils extends Runnable {
    void run(Object obj);
}
