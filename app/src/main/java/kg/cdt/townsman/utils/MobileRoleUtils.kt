package kg.cdt.townsman.utils

import com.blankj.utilcode.util.StringUtils
import kg.cdt.townsman.wsapi.model.UserRolesEnum

class MobileProfileUtils {
    companion object {
        private var CURRENT_ROLE = ""

//        fun isMiddle(): Boolean {
//            if (CURRENT_ROLE.isNotEmpty()) {
//                return StringUtils.equalsIgnoreCase(CURRENT_ROLE, UserRolesEnum.middle.toString())
//            }
//            return false
//        }

//        fun isJunior(): Boolean {
//            if (CURRENT_ROLE.isNotEmpty()) {
//                return StringUtils.equalsIgnoreCase(CURRENT_ROLE, UserRolesEnum.junior.toString())
//            }
//            return false
//        }

        fun isDefault(): Boolean {
            if (CURRENT_ROLE.isNotEmpty()) {
                return StringUtils.equalsIgnoreCase(CURRENT_ROLE, UserRolesEnum.default.toString())
            }
            return false
        }

        fun detectMobileProfile(): String {
            return when {
//                isMiddle() -> UserRolesEnum.middle.toString()
//                isJunior() -> UserRolesEnum.junior.toString()
                isDefault() -> UserRolesEnum.default.toString()
                else -> String()
            }
        }
    }

    fun init() {}

    init {
        CURRENT_ROLE = StorageUtils().getCurrentRole()
    }
}