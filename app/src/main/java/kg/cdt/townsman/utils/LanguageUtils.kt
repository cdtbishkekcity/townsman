package kg.cdt.townsman.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.wsapi.model.UserLanguages
import kg.cdt.townsman.R
import java.util.*

class LanguageUtils(val activity: Activity): DialogFragment(), RadioGroup.OnCheckedChangeListener {
    private val TAG = this.javaClass.name
    private lateinit var ctx: Context
    private var idRu: Int = 6
    private var idKy: Int = 6

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(activity)
        val layout = inflater.inflate(R.layout.dialog_lang, null)
        val langRu = layout.findViewById<RadioButton>(R.id.ru_lang)
        val langKy = layout.findViewById<RadioButton>(R.id.ky_lang)
        val okButton = layout.findViewById<TextView>(R.id.ok_button)
        val cancelButton = layout.findViewById<TextView>(R.id.cancel_button)

        langRu.setOnClickListener { idRu = 0 }
        langKy.setOnClickListener { idKy = 1 }

        okButton.setOnClickListener {
            if (idRu == 0) {
                langKy.isChecked = false
                langRu.isChecked = true
                setLocate(UserLanguages.RU.toString(), ctx)
                activity.finish()
                activity.startActivity(Intent(context, activity::class.java))
            } else if (idKy == 1) {
                langKy.isChecked = true
                langRu.isChecked = false
                setLocate(UserLanguages.KY.toString(), ctx)
                activity.finish()
                activity.startActivity(Intent(context, activity::class.java))
            }
            dismiss()
        }

        cancelButton.setOnClickListener {
            dismiss()
        }

        if (checkedItem() == 0) {
            langRu.isChecked = true
        } else if (checkedItem() == 1) {
            langKy.isChecked = true
        }

        ctx = context!!

        val builder = android.app.AlertDialog.Builder(activity)
        builder.setView(layout)
        val alert = builder.create() as android.app.AlertDialog
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        isCancelable = true
        alert.setCanceledOnTouchOutside(true)
        return alert
    }


    fun setLocate(Lang: String, baseContext: Context) {
        try {
            val locale = Locale(Lang)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)

            Prefs.putString(StorageUtils.LANGUAGE, Lang)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    fun loadLocate(context: Context) {
        try {
            setLocate(Prefs.getString(StorageUtils.LANGUAGE, ""), context)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    private fun checkedItem(): Int {
        val getPrefsLanguage = Prefs.getString(StorageUtils.LANGUAGE, "")
        try {
            if (Prefs.contains(StorageUtils.LANGUAGE)) {
                if (getPrefsLanguage == UserLanguages.RU.toString()) {
                    return 0
                } else if (getPrefsLanguage == UserLanguages.KY.toString()) {
                    return 1
                }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return -1
    }

    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
    }
}

