package kg.cdt.townsman.utils

import android.content.Context
import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.shared_prefs.User
import kg.cdt.townsman.wsapi.model.UprMobileAppSettings

class StorageUtils {
    private val TAG = this.javaClass.name

    private val gsonUtils = GsonUtils()

    companion object {
        const val LOGIN_STATE = "login_state"
        const val SESSION_HEADER = "SESSION_HEADER"
        const val LANGUAGE = "language"
        const val CURRENT_ACTIVITY = "CURRENT_ACTIVITY"
        const val CURRENT_ROLE = "current_role"
        const val PUSH_TOKEN = "PUSH_TOKEN"

        const val USER = "USER"

        const val MOBILE_APP_SETTINGS = "MOBILE_APP_SETTINGS"
        const val ROTATE_MAP_BY_COMPASS_ENABLED = "rotate_map_by_compass_enabled"
    }

    private fun prefsBuilder(context: Context, packageName: String) {
        Prefs.Builder()
            .setContext(context)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }

    fun init(context: Context, packageName: String) {
        try {
            if (Prefs.getPreferences() == null) {
                prefsBuilder(context, packageName)
            }
        } catch (e: Exception) {
            try {
                prefsBuilder(context, packageName)
            } catch (e: Exception) {
                LogUtils.error(TAG, e.message)
            }
        }
    }

    /* storage helpers */
    fun saveSessionHeader(sessionHeader: String) = Prefs.putString(SESSION_HEADER, sessionHeader)

    fun getSessionHeader() = Prefs.getString(SESSION_HEADER, "")
    fun removeSessionHeader() = Prefs.remove(SESSION_HEADER)

    fun saveLoginState(json: String) = Prefs.putString(LOGIN_STATE, json)
    fun getLoginState() = Prefs.getString(LOGIN_STATE, "")
    fun removeLoginState() = Prefs.remove(LOGIN_STATE)

    fun saveCurrentActivity(json: String) = Prefs.putString(CURRENT_ACTIVITY, json)
    fun getCurrentActivity() = Prefs.getString(CURRENT_ACTIVITY, "")
    fun removeCurrentActivity() = Prefs.remove(CURRENT_ACTIVITY)

    fun saveCurrentRole(string: String) = Prefs.putString(CURRENT_ROLE, string)
    fun getCurrentRole() = Prefs.getString(CURRENT_ROLE, "")
    fun removeCurrentRole() = Prefs.remove(CURRENT_ROLE)

    fun savePushToken(string: String) = Prefs.putString(PUSH_TOKEN, string)
    fun getPushToken() = Prefs.getString(PUSH_TOKEN, "")
    fun removePushToken() = Prefs.remove(PUSH_TOKEN)


    fun saveUserAppSettingsIfDiff(getAppSettings: UprMobileAppSettings?) {
        val toJsonUserAppSettings = gsonUtils.toJson(getAppSettings ?: UprMobileAppSettings())
        if (getUserAppSettings() != toJsonUserAppSettings) {
            LogUtils.info(TAG, "SAVE UserAppSettings: $getAppSettings")

            saveUserAppSettings(toJsonUserAppSettings)
        }
    }

    fun saveUserAppSettings(json: String) = Prefs.putString(MOBILE_APP_SETTINGS, json)
    fun getUserAppSettings() = Prefs.getString(MOBILE_APP_SETTINGS, gsonUtils.toJson(
        UprMobileAppSettings()
    ))
    fun removeUserAppSettings() = Prefs.remove(MOBILE_APP_SETTINGS)

    fun saveUserData(json: String) = Prefs.putString(USER, json)
    fun getUserData() = Prefs.getString(USER, gsonUtils.toJson(User()))
    fun removeUserData() = Prefs.remove(USER)

}