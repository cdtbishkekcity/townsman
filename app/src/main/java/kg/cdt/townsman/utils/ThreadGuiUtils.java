package kg.cdt.townsman.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadGuiUtils {
    public static Handler handler;
    private static ExecutorService threadPoolExecutor;
    public static int MaxThreadsPools = 9;

    static {
        threadPoolExecutor = Executors.newFixedThreadPool(MaxThreadsPools);
    }

    public static Handler getHandler() {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        return handler;
    }

    public static boolean doInPostHandler(Runnable runnable) {
        return getHandler().post(runnable);
    }

    public static Future<?> doInThreadPool(Runnable runnable) {
        if (runnable != null) {
            return threadPoolExecutor.submit(runnable);
        }
        return null;
    }

    public static void doInNewThread(Runnable runnable) {
        if (runnable != null) {
            new Thread(runnable).start();
        }
    }

    public static void doInNewThreadOnPostHandler(final Runnable runnable) {
        if (runnable != null) {
            new Thread(() -> doInPostHandler(runnable)).start();
        }
    }

    public static Future<?> doInThreadPoolOnPostHandler(final Runnable runnable) {
        if (runnable != null) {
            return threadPoolExecutor.submit(() -> doInPostHandler(runnable));
        }
        return null;
    }

    public static boolean doInPostDelayedHandler(
            final Runnable runnable,
            final long milliseconds
    ) {
        if (runnable != null) {
            return getHandler().postDelayed(runnable, milliseconds);
        }
        return false;
    }

    public static void runOnUiThread(
            Activity activity,
            Runnable runnable
    ) {
        if (activity != null && runnable != null) {
            activity.runOnUiThread(runnable);
        }
    }

    public static void initCachedThreadsInPool() {
        for (int i = 0; i < MaxThreadsPools; i++) {
            doInThreadPool(() -> {
                try {
                    Thread.sleep(99);
                } catch (InterruptedException ignored) {
                }
            });
        }
    }
}
