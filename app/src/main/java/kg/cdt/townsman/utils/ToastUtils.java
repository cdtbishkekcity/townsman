package kg.cdt.townsman.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtils {
    public static void ToastLong(Context context, final String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void ToastLongInGuiThread(final Context context, final String message) {
        ThreadGuiUtils.doInPostHandler(new Runnable() {
            @Override
            public void run() {
                ToastLong(context, message);
            }
        });
    }

    public static void ToastShort(Context context, final String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void ToastShortInGuiThread(final Context context, final String text) {
        ThreadGuiUtils.doInPostHandler(new Runnable() {
            @Override
            public void run() {
                ToastShort(context, text);
            }
        });
    }
}
