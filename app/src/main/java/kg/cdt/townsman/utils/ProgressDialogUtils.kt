package kg.cdt.townsman.utils

import android.app.ProgressDialog
import android.content.Context

class ProgressDialogUtils {
    private val TAG = this.javaClass.name

    fun show(
        ctx: Context,
        message: String = "",
        cancelable: Boolean = false
    ): ProgressDialog? {
        try {
            val progressDialog: ProgressDialog? = ProgressDialog(ctx)
            progressDialog?.run {
                setMessage(message)
                setCancelable(cancelable)
                show()
            }

            return progressDialog
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return null
    }
}