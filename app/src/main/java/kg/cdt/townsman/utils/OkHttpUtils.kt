package kg.cdt.townsman.utils

import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.util.concurrent.TimeUnit

class OkHttpUtils {
    private val TAG = this.javaClass.name

    companion object {
        var ThrownError = true
        var LogError = true

        var MAX_IDLE_CONNECTIONS = 30
        var KEEP_ALIVE_DURATION_MS = 30L
        var ReadTimeout = 60000L
        var ConnectTimeout = 10000L

        val HTTP_HEADERS = hashMapOf<String, String>()

        val MEDIA_TYPE_APPLICATION_URLENCODED =
            "application/x-www-form-urlencoded; charset=utf-8".toMediaTypeOrNull()
        val MEDIA_TYPE_APPLICATION_JSON = "application/json; charset=utf-8".toMediaTypeOrNull()

        var ConnectionPool = ConnectionPool(MAX_IDLE_CONNECTIONS, KEEP_ALIVE_DURATION_MS, TimeUnit.MINUTES)
        var okHttpClient: OkHttpClient = init()

        @Synchronized
        private fun init(): OkHttpClient {
            return OkHttpClient.Builder()
                .readTimeout(ReadTimeout, TimeUnit.MILLISECONDS)
                .connectTimeout(ConnectTimeout, TimeUnit.MILLISECONDS)
                .connectionPool(ConnectionPool)
                //.addInterceptor(HttpLoggingInterceptor())
                .build()
        }

        @Synchronized
        fun reInit() {
            okHttpClient.connectionPool.evictAll()
            ConnectionPool = ConnectionPool(MAX_IDLE_CONNECTIONS, KEEP_ALIVE_DURATION_MS, TimeUnit.MINUTES)
            okHttpClient = init()
        }

        fun convertResponseBodyToString(responseBody: ResponseBody?): String? {
            return responseBody?.string()
        }

        fun convertResponseBodyToBytes(responseBody: ResponseBody?): ByteArray? {
            return responseBody?.bytes()
        }


        /**
         * http auth header methods
         */
        var useSetCookieHeaderFromResponse = true

        // WWW-Authenticate: Basic realm="Realm"
        var DetectedAuthenticateBasicRealm = false
        const val AuthenticateBasicRealmHeader = "WWW-Authenticate"
        const val AuthenticateBasicRealmHeaderValue = "Basic realm=\"Realm\""

        const val AuthorizationHeader = "Authorization"
        const val SetCookieHeader = "Set-Cookie"
        const val CookieHeader = "Cookie"

        fun addCredentialsBasicHeader(login: String, password: String) {
            HTTP_HEADERS[AuthorizationHeader] = Credentials.basic(login, password)
        }

        fun removeCredentialsBasicHeader() {
            HTTP_HEADERS.remove(AuthorizationHeader)
        }

        fun removeSetCookieHeader() {
            HTTP_HEADERS.remove(SetCookieHeader.toLowerCase())
        }

        fun setCookieAfterAuthAndRemoveCredentials() {
            HTTP_HEADERS[CookieHeader] = HTTP_HEADERS[SetCookieHeader.toLowerCase()].toString()

            removeCredentialsBasicHeader()
            removeSetCookieHeader()
        }

        fun getAuthSessionToken() = HTTP_HEADERS[CookieHeader].toString().split(";")[0]

        fun setAuthSessionToken(token: String) {
            HTTP_HEADERS[CookieHeader] = token
        }

        fun removeAuthSessionHeader() = HTTP_HEADERS.remove(CookieHeader)
    }

    fun tryGet(
        url: String,
        tryNumber: Int = 2,
        sleepInCatch: Long = 3000
    ): String? {
        if (url.isEmpty()) {
            return ""
        }

        for (i in 1..tryNumber) {
            try {
                return get(url)
            } catch (e: Exception) {
                if (LogError) {
                    LogUtils.error(TAG, "tryGet attempt $i, ${e.message}")
                }

                if (i < tryNumber) {
                    Thread.sleep(sleepInCatch)
                } else {
                    if (ThrownError) {
                        throw IllegalStateException(e)
                    }
                }
            }
        }
        return ""
    }

    fun tryPost(
        url: String,
        paramStr: String,
        tryNumber: Int = 2,
        sleepInCatch: Long = 3000,
        media_type: MediaType? = MEDIA_TYPE_APPLICATION_URLENCODED
    ): String? {
        if (url.isEmpty()) {
            return ""
        }

        for (i in 1..tryNumber) {
            try {
                return post(url, paramStr, media_type)
            } catch (e: Exception) {
                if (LogError) {
                    LogUtils.error(TAG, "tryPost attempt $i, ${e.message}")
                }

                if (i < tryNumber) {
                    Thread.sleep(sleepInCatch)
                } else {
                    if (ThrownError) {
                        throw IllegalStateException(e)
                    }
                }
            }
        }
        return ""
    }

    fun get(url: String): String? {
        return execute(createGetRequest(url)).body?.string()
    }

    fun post(
        url: String,
        paramStr: String,
        media_type: MediaType?
    ): String? {
        return execute(createPostBody(url, paramStr, media_type)).body?.string()
    }

    fun execute(request: Request): Response {
        val execute = okHttpClient
            .newCall(request)
            .execute()

        // handle COOKIE response headers
        DetectedAuthenticateBasicRealm = false

        val headers = execute.headers
        if (headers.size > 0) {
            for (name in headers.names().iterator()) {
                if (useSetCookieHeaderFromResponse && name.toLowerCase() == SetCookieHeader.toLowerCase()) {
                    HTTP_HEADERS[name.toLowerCase()] = headers.get(name.toLowerCase()) ?: ""
                }
                if (name.toLowerCase() == AuthenticateBasicRealmHeader.toLowerCase()) {//WWW-Authenticate: Basic realm="Realm"
                    if ((headers.get(name) ?: "").contains(AuthenticateBasicRealmHeaderValue)) {
                        DetectedAuthenticateBasicRealm = true
                    }
                }
            }
        }

        return execute
    }

    fun createPostBody(
        url: String,
        paramStr: String,
        media_type: MediaType?
    ): Request {
        val body = RequestBody.create(media_type, paramStr)
        val build = Request.Builder()
            .url(url)
            .post(body)

        if (HTTP_HEADERS.isNotEmpty()) {
            for (httpHeader in HTTP_HEADERS) {
                build.addHeader(httpHeader.key, httpHeader.value)
            }
        }

        return build.build()
    }

    fun createGetRequest(url: String): Request {
        val build = Request.Builder()
            .url(url)

        if (HTTP_HEADERS.isNotEmpty()) {
            for (httpHeader in HTTP_HEADERS) {
                build.addHeader(httpHeader.key, httpHeader.value)
            }
        }

        return build.build()
    }
}