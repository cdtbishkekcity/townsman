package kg.cdt.townsman.utils

import android.util.Log
import com.blankj.utilcode.util.TimeUtils

object LogUtils {
    var TEST_MODE = false

    fun info(tag: String, info: String?) {
        if (!TEST_MODE) {
            Log.i(tag, "[${TimeUtils.getNowString()}] $info")
        } else {
            println("[${TimeUtils.getNowString()}] $info")
        }
    }

    fun error(tag: String, error: String?) {
        if (!TEST_MODE) {
            Log.e(tag, "[${TimeUtils.getNowString()}] $error")
        } else {
            println("[${TimeUtils.getNowString()}] $error")
        }
    }

    fun error(tag: String, error: String?, e: Exception?) {
        if (!TEST_MODE) {
            Log.e(tag, "[${TimeUtils.getNowString()}] $error", e)
        } else {
            println("[${TimeUtils.getNowString()}] $error")
        }
    }

    fun warning(tag: String, warning: String?) {
        if (!TEST_MODE) {
            Log.w(tag, "[${TimeUtils.getNowString()}] $warning")
        } else {
            println("[${TimeUtils.getNowString()}] $warning")
        }
    }
}