package kg.cdt.townsman.utils

object RandomHelper {
    val TAG = this.javaClass.name

    fun generate(stringLength: Int = 5): String {
        val charPool: List<Char> = ('A'..'Z') + ('0'..'9')
        val alphanumericRegex = "[A-Z0-9]+"
        var randomString = ""

        try {
            randomString = (1..stringLength)
                .map { kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("")

            //assert(randomString.matches(Regex(alphanumericRegex)))
        } catch (e: Exception) {
            print("$TAG: ${e.message}")
        }
        return randomString
    }
}