package kg.cdt.townsman.utils

import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.DeviceUtils

object AppAndroidVersionHelper {
    val androidNullVresion = "Android Y.Y.Y (SomeManufacture)"
    val toRegex by lazy { """Android\s([\d.]{1,3})""".toRegex() }

    fun getVersionString(): String {
        // 0.7.1030.1957, Android 8.1.0 (MiA1)
        return "${AppUtils.getAppVersionName()}, Android ${DeviceUtils.getSDKVersionName()} (${DeviceUtils.getModel()})"
    }

    fun getAppVersion() = AppUtils.getAppVersionName()

    fun getOsVersion() = "Android ${DeviceUtils.getSDKVersionName()} (${DeviceUtils.getModel()})"

    fun detectVersion(verString: String): String {
        if (verString.isEmpty()) {
            return "Y.Y.Y"
        }
        return toRegex.find(verString, 0)?.groupValues?.get(1) ?: ""
    }
}