package kg.cdt.townsman.utils

object PhoneNumberConst {
    const val helpLine = "111"
    const val police = "102"
    const val ambulance = "103"
}