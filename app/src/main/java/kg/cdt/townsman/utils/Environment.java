package kg.cdt.townsman.utils;

public class Environment {
    public static final String NL = "\n";
    public static final String NL1 = "\r\n";
    public static final String NL2 = System.getProperty("line.separator");
}
