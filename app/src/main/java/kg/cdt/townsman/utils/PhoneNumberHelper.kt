package kg.cdt.townsman.utils

import android.annotation.SuppressLint
import android.widget.TextView
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

class PhoneNumberHelper {
    private val TAG = this.javaClass.name
    private val phoneMaskFormat = "996_________"
    val phoneMaskFormat2 = "996__________"

    @SuppressLint("MissingPermission")
    fun getPhoneNumber(): String {
        var phoneNumber = ""

//        try {
//            val phoneStatus = PhoneUtils.getPhoneStatus()
//            if (!TextUtils.isEmpty(phoneStatus)) {
//                val split = phoneStatus.split("\n")
//                for (s in split) {
//                    if (s.contains("Line1Number = ")) {
//                        val split1 = s.split(" = ")
//                        phoneNumber = split1[1].trim()
//
//                        break
//                    }
//
//                    if (!TextUtils.isEmpty(phoneNumber)) {
//                        break
//                    }
//                }
//            }
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }

        return phoneNumber
    }

    fun phoneMask(phone: TextView, phoneMaskFormat2: String) {
        try {
            val slots = UnderscoreDigitSlotsParser().parseSlots(phoneMaskFormat)
            val formatWatcher = MaskFormatWatcher(MaskImpl.createTerminated(slots))
            formatWatcher.installOn(phone)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }
}