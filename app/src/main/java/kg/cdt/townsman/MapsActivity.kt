package kg.cdt.townsman

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.ToastUtils
import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.common_permissions.MultiPermissionsRequest
import kg.cdt.townsman.geo.GeoGpsConnector
import kg.cdt.townsman.geo.osm.OsmConst
import kg.cdt.townsman.geo.osm.OsmHelper.Companion.setGeoAnimate2OSM
import kg.cdt.townsman.geo.osm.OsmHelper.Companion.setMarker2OSM
import kg.cdt.townsman.utils.*
import kg.cdt.townsman.utils.ThreadGuiUtils.doInPostHandler
import kg.cdt.townsman.utils.ThreadGuiUtils.doInThreadPool
import kg.cdt.townsman.utils.ToastUtils.ToastShort
import kg.cdt.townsman.wsapi.model.UprMobileAppSettings
import kotlinx.android.synthetic.main.activity_maps.*
import org.osmdroid.config.Configuration
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import java.io.File
import kotlin.math.roundToLong

class MapsActivity : AppCompatActivity(), SensorEventListener {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    //geo value
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private val roundAfterPoint: Double = 1000000.0
    private var enableDefaultFunctionOnClickButton: Boolean = false

    //Enabled zoom all geo point
    private var enabledZoomAllGeoPoint: Boolean = false

    //compass manager
    private var rotateMapByCompassEnabled = false
    private var mSensorManager: SensorManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        //check permission
        checkPermissions()

        val userAppSettings = gsonUtils.fromJson(
            StorageUtils().getUserAppSettings(),
            UprMobileAppSettings::class.java
        )
        GeoGpsConnector.MIN_DISTANCE_UPDATES = userAppSettings.geo_min_distance.toFloat()
        GeoGpsConnector.MIN_TIME_UPDATES = userAppSettings.geo_min_time * 1000
        GeoGpsConnector.setContext(this@MapsActivity)

        val osmConfig = Configuration.getInstance()
        osmConfig.userAgentValue = packageName
        val basePath = File(cacheDir.absolutePath, "osmdroid")
        osmConfig.osmdroidBasePath = basePath
        val tileCache = File(osmConfig.osmdroidBasePath, "tile")
        osmConfig.osmdroidTileCache = tileCache
        try {
            GeoGpsConnector.onGpsLocationChanged2 = object : RunnableUtils {
                override fun run(obj: Any?) {
                    if (obj is Location && obj.provider == LocationManager.GPS_PROVIDER) {

                        latitude = (obj.latitude * roundAfterPoint).roundToLong()
                            .toDouble() / roundAfterPoint
                        longitude = (obj.longitude * roundAfterPoint).roundToLong()
                            .toDouble() / roundAfterPoint

                        val gpsLatLong = "\ngpsLat: $latitude \ngpsLon: $longitude"

                        LogUtils.info(TAG, gpsLatLong)

                        val geoPoint = GeoPoint(
                            obj.latitude,
                            obj.longitude
                        )
                        if (this@MapsActivity.window.decorView.rootView.isShown) {
                            doInPostHandler {
                                if (!enabledZoomAllGeoPoint) {
                                    setGeoAnimate2OSM(mapOsm, geoPoint, false)
                                }
                                addMarkerSelf()
                            }
                        }
                    }
                }

                override fun run() {}
            }

            mapOsm.run {
                setTileSource(OsmConst.tileSourceDefault)
                zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
                setMultiTouchControls(true)
                setGeoAnimate2OSM(mapOsm, OsmConst.GeoDefaultBishkek, true)
                ToastShort(
                    this@MapsActivity,
                    getString(R.string.search_current_location)
                )

                // detect self geo position
                doInThreadPool {
                    for (i in 1..70)
                        try {
                            Thread.sleep(3 * 1000L)

                            if (geoPointIsNotNull()) {
                                latitude = (latitude * roundAfterPoint).roundToLong()
                                    .toDouble() / roundAfterPoint
                                longitude = (longitude * roundAfterPoint).roundToLong()
                                    .toDouble() / roundAfterPoint

                                break
                            }
                        } catch (e: Exception) {
                            LogUtils.error(TAG, e.message, e)
                        }
                }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)

            PrettyDialogUtils().showError(
                this,
                getString(R.string.error_of_map_dialog_title_text),
                "${getString(R.string.error_on_startup_of_map)} (${e.message})"
            )
        }

        self_marker.setOnClickListener {
            try {
                enabledZoomAllGeoPoint = false
                if (latitude > 0 && longitude > 0) {
                    val geoPoint = GeoPoint(
                        latitude,
                        longitude
                    )
                    setGeoAnimate2OSM(mapOsm, geoPoint, false)
                } else {
                    enabledZoomAllGeoPoint = false
                    val geoPoint = GeoPoint(
                        OsmConst.GeoDefaultBishkek.latitude,
                        OsmConst.GeoDefaultBishkek.longitude
                    )
                    setGeoAnimate2OSM(mapOsm, geoPoint, false)
                    ToastShort(
                        this@MapsActivity,
                        getString(R.string.search_current_location)
                    )
                }
            } catch (e: Exception) {
                LogUtils.error(TAG, e.message, e)
                PrettyDialogUtils().showError(
                    this,
                    getString(R.string.error_of_map_dialog_title_text),
                    "${getString(R.string.error_on_startup_of_map)} (${e.message})"
                )
            }
        }

        //init compass
        compass()
    }

    private fun checkPermissions() {
        try {
            // check permissions
            if (!MultiPermissionsRequest(this).permissionsRequest(this)) {
                return
            }
            // check on wifi or mobile is enabled
            if (NetworkUtil.getConnectivityStatus(this) == 2 && !NetworkUtil.internetCheckAvailability(this) ||
                NetworkUtil.getConnectivityStatus(this) == 0) {
                if (NetworkUtils.getMobileDataEnabled().not()) {
                    MultiPermissionsRequest(this).openMobileOrWifiSettings(
                        this,
                        mobileSettings = true
                    )
                }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    override fun onResume() {
        checkPermissions()
        super.onResume()

        // for the system's orientation sensor registered listeners
        mSensorManager?.registerListener(
            this,
            mSensorManager?.getDefaultSensor(Sensor.TYPE_ORIENTATION),
            SensorManager.SENSOR_DELAY_GAME
        )
    }

    override fun onDestroy() {
        stopInstances()
        super.onDestroy()
    }

    override fun onBackPressed() {
        try {
            stopInstances()

            finish()
            startActivity(Intent(this, MainActivity::class.java))
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    private fun stopInstances() {
        try {
            // clear always display on
            runOnUiThread {
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }

            GeoGpsConnector.getInstance().stop()
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    private fun addMarkerSelf() {
        try {
            if (geoPointIsNotNull()) {
                mapOsm.overlays.clear()
                setMarker2OSM(
                    this@MapsActivity,
                    mapOsm, GeoPoint(
                        latitude,
                        longitude
                    ),
                    R.drawable.marker_default,
                    getString(R.string.your_location)
                )
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }

    private fun geoPointIsNotNull(): Boolean {
        try {
            if (GeoGpsConnector.getInstance().lastKnownPosition.latitude > 0.0
                && GeoGpsConnector.getInstance().lastKnownPosition.longitude > 0.0
                && OsmConst.GeoDefaultBishkek.latitude != GeoGpsConnector.getInstance().lastKnownPosition.latitude
                && OsmConst.GeoDefaultBishkek.longitude != GeoGpsConnector.getInstance().lastKnownPosition.longitude
            ) {
                return true
            }
            return false
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
        return false

    }
    @SuppressLint("RestrictedApi")
    private fun compass() {
        val pm = packageManager
        if (!pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)) {
            compass_button.visibility = View.GONE
        }
        rotateMapByCompassEnabled = Prefs.getString(
            StorageUtils.ROTATE_MAP_BY_COMPASS_ENABLED,
            true.toString()
        ) == true.toString()

        // unlock and awake screen
        ScreenUtils.setKeepScreenOn(window)

        // initialize your android device sensor capabilities
        mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager

        compass_button.setOnClickListener {
            try {
                if (enableDefaultFunctionOnClickButton) {
                    rotateMapByCompassEnabled = !rotateMapByCompassEnabled

                    // save enabled|disabled rotation mode 2 storage
                    Prefs.putString(
                        StorageUtils.ROTATE_MAP_BY_COMPASS_ENABLED,
                        rotateMapByCompassEnabled.toString()
                    )

                    if (rotateMapByCompassEnabled) {
                        ToastUtils.showLong(getString(R.string.rotate_map_enabled))
                    } else {
                        // restore 0 degrees om osm map
                        mapOsm.mapOrientation = 0f
                        ToastUtils.showLong(getString(R.string.rotate_map_disabled))
                    }
                }
            } catch (e: Exception) {
                LogUtils.error(TAG, e.message, e)
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
    }

}