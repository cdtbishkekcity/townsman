package kg.cdt.townsman.common_permissions

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Context.POWER_SERVICE
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.PowerManager
import android.provider.Settings

class PowerServiceHelper {
    companion object {
        @SuppressLint("BatteryLife")
        fun doPower(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val packageName = activity.packageName
                val pm = activity.getSystemService(Context.POWER_SERVICE) as PowerManager
                if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                    //some device doesn't has activity to handle this intent
                    //so add try catch
                    val intent = Intent()
                    intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                    intent.data = Uri.parse("package: $packageName")
                    activity.startActivity(intent)
                }
            }
        }

        fun isInDozeWhiteList(context: Context): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val powerManager = context.getSystemService(PowerManager::class.java)
                return powerManager!!.isIgnoringBatteryOptimizations(context.packageName)
            }
            return false
        }

        @SuppressLint("BatteryLife")
        fun requestChangeBatteryOptimizations(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val intent = Intent()
                val packageName = activity.packageName
                val pm = activity.getSystemService(Context.POWER_SERVICE) as PowerManager
                if (pm.isIgnoringBatteryOptimizations(packageName)) {
                    intent.action = Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS
                } else {
                    intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                    intent.data = Uri.parse("package:$packageName")
                }
                activity.startActivity(intent)
            }
        }

        fun isIdleMode(context: Context): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val packageName = context.packageName
                val manager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
                val isIgnoringOptimizations = manager.isIgnoringBatteryOptimizations(packageName)
                return manager.isDeviceIdleMode && !isIgnoringOptimizations
            }
            return false
        }

        @SuppressLint("BatteryLife")
        fun startLocationService(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Before we start the service, confirm that we have extra power usage privileges.
                val pm = context.getSystemService(POWER_SERVICE) as PowerManager
                val intent = Intent()
                if (!pm.isIgnoringBatteryOptimizations(context.packageName)) {
                    intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS

                    intent.data = Uri.parse("package:" + context.packageName)
                    context.startActivity(intent)
                }
                //startService(Intent(this, TrackerService::class.java))
            }
        }
    }
}