package kg.cdt.townsman.common_permissions

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.Settings
import android.view.Gravity
import androidx.core.app.ActivityCompat
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.PermissionUtils
import kg.cdt.townsman.R
import kg.cdt.townsman.geo.GeoGpsConnector
import kg.cdt.townsman.geo.GeoPermissionsSettings
import kg.cdt.townsman.utils.LogUtils
import kg.cdt.townsman.utils.PrettyDialogUtils
import kg.cdt.townsman.utils.ThreadGuiUtils.doInPostDelayedHandler
import libs.mjn.prettydialog.PrettyDialog

class MultiPermissionsRequest(val context: Context) {
    private val TAG = this.javaClass.name

    private val permissionsAndroid = arrayOf(Manifest.permission.INTERNET)


    private fun isAllGranted(): Boolean {
        return PermissionUtils.isGranted(*permissionsAndroid)
    }

    @SuppressLint("WrongConstant")
    private fun request(): MultiPermissionsRequest {
        try {
            PermissionUtils.permission(*permissionsAndroid).apply {
                callback(object : PermissionUtils.FullCallback {
                    override fun onGranted(granted: MutableList<String>) {
                        LogUtils.info(TAG, "permissionsGranted: ${granted.toString()}")
                    }

                    override fun onDenied(
                        deniedForever: MutableList<String>,
                        denied: MutableList<String>
                    ) {
                        LogUtils.info(TAG, "permissionsDenied: ${denied.toString()}")
                    }
                })

                request()
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }

        return this
    }

    fun permissionsRequest(context: Context): Boolean {
/*        GeoGpsConnector.setContext(context)
        if (!GeoGpsConnector.geoPermissionIsDenied(context) && !GeoGpsConnector.getInstance().isEnabledGps) {
            LogUtils.error(TAG, "geodenied")
            requestGeoPermissionDialog(context)
            return false
        }*/

        if (MultiPermissionsRequest(context).request().isAllGranted().not()) {
            return false
        }
        return true
    }

    fun checkPermissionGeoLocation(context: Context, activity: Activity): Boolean {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            ActivityCompat.requestPermissions(activity, permissions, 0)
            return false
        }

        GeoGpsConnector.setContext(context)
        if (!GeoGpsConnector.geoPermissionIsDenied(context) && !GeoGpsConnector.getInstance().isEnabledGps) {
            LogUtils.error(TAG, "geodenied")
            requestGeoPermissionDialog(context)
            return false
        }
        return true
    }

    private fun requestGeoPermissionDialog(context: Context) {
        try {
            if (!GeoPermissionsSettings.dialogIsRunning) {
                val pDialog = PrettyDialog(context)
                pDialog
                    .setTitleColor(R.color.pdlg_color_red)
                    .setMessage(context.getString(R.string.geo_service_disabled_dlg))
                    .setMessageColor(R.color.pdlg_color_red)
                    .setIcon(R.drawable.pdlg_icon_info)
                    .setIconTint(R.color.pdlg_color_red)
                    .addButton(
                        context.getString(R.string.run_geo_settings),
                        R.color.pdlg_color_black,
                        android.R.color.white
                    ) {
                        GeoGpsConnector.callGeoSystemSettings(context)

                        pDialog.dismiss()

                        GeoPermissionsSettings.dialogIsRunning = false
                    }
                    .setGravity(Gravity.CENTER)
                pDialog.setOnCancelListener {
                    GeoPermissionsSettings.dialogIsRunning = false
                }
                pDialog.setCancelable(false)
                pDialog.show()

                GeoPermissionsSettings.dialogIsRunning = true
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }
    }

    fun openMobileOrWifiSettings(
        context: Context,
        mobileSettings: Boolean = false,
        wifiSetting: Boolean = false
    ) {
        doInPostDelayedHandler({
            var title: String = context.getString(R.string.internet_disabled_title)
            var msg: String = context.getString(
                R.string.enable_internet_msg,
                AppUtils.getAppName(),
                context.getString(R.string.enable_internet_title)
            )
            var btnTitle: String = context.getString(R.string.enable_internet_title)
            if (wifiSetting) {
                title = context.getString(R.string.wifi_disabled_title)
                msg = context.getString(
                    R.string.wifi_internet_msg,
                    AppUtils.getAppName(),
                    context.getString(R.string.wifi_internet_title)
                )
                btnTitle = context.getString(R.string.wifi_internet_title)
            }
            try {
                PrettyDialogUtils().showError(
                    context,
                    title,
                    msg,
                    buttonTitle = btnTitle,
                    clickHandler = Runnable {
                        try {
                            val intent = Intent(
                                if (mobileSettings) {
                                    Settings.ACTION_DATA_ROAMING_SETTINGS
                                } else {
                                    if (wifiSetting) {
                                        Settings.ACTION_WIFI_SETTINGS
                                    } else {
                                        Settings.ACTION_DATA_ROAMING_SETTINGS
                                    }
                                }
                            )
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            context.startActivity(intent)
                        } catch (e: Exception) {
                            LogUtils.error(TAG, e.message)

                            PrettyDialogUtils().showError(
                                context,
                                "",
                                e.message ?: ""
                            )
                        }
                    }
                )

            } catch (e: Exception) {
                LogUtils.error(TAG, e.message)

                PrettyDialogUtils().showError(
                    context,
                    "",
                    e.message ?: ""
                )
            }
        }, 500L)
    }

}