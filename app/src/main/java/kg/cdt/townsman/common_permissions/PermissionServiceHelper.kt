package kg.cdt.townsman.common_permissions

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings

class PermissionServiceHelper {
    companion object {
        fun isDrawOverlayPermission(activity: Activity): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(activity)) {
                    val intent = Intent(
                        Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + activity.packageName)
                    )
                    activity.startActivity(intent)
                    false
                } else {
                    true
                }
            } else {
                true
            }
        }

    }
}