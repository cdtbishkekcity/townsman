package kg.cdt.townsman.wsapi.model

data class UserGeoPointInfo(
    var user_id: Long? = null,
    var latitude: String? = null,
    var longitude: String? = null,
    var actual_created: String? = null,
    var created: String? = null
)