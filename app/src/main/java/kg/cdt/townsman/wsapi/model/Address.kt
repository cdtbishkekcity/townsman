package kg.cdt.townsman.wsapi.model

import kg.cdt.townsman.utils.JSONConvertable

data class Address(
    val house_number: String? = null,
    val road: String? = null,
    val postcode: String? = null,
    val city: String? = null,
    val country: String? = null,
    val country_code: String? = null
): JSONConvertable