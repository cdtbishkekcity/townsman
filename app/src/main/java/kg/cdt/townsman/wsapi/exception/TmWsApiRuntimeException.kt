package kg.cdt.townsman.wsapi.exception

class TmWsApiRuntimeException(message: String?) : RuntimeException(message)