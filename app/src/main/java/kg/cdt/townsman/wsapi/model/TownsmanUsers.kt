package kg.cdt.townsman.wsapi.model

import kg.cdt.townsman.utils.JSONConvertable

data class TownsmanUsers(
    var id: Long = 0L,
    var uuid: String? = null,
    var login: String = "",
    var sms_code: String = "",
    var second_name: String? = null,
    var name: String? = null,
    var last_name: String? = null,
    var pin: String? = null,
    var email: String? = null,
    var registered_push_token: String? = null,
    var app_version: String? = null,
    var os_version: String? = null,
    var remote_ip: String? = null,
    var last_activity: String? = null,
    var last_login: String? = null,
    var created: String? = null,
    var deleted: Boolean = false
) : JSONConvertable
