package kg.cdt.townsman.wsapi.model

data class UserGeoHistoryPoint(
    var latitude: String? = null,
    var longitude: String? = null,
    var actual_created: String? = null
)