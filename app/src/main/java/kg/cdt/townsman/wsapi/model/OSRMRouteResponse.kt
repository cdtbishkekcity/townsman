package kg.cdt.townsman.wsapi.model

import java.util.*

data class OSRMRouteResponse(
    var route: MutableList<List<String>> = ArrayList(),
    var distance: String? = null,
    var duration: String? = null
)