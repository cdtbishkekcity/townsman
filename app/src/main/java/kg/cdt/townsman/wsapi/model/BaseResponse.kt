package kg.cdt.townsman.wsapi.model

import kg.cdt.townsman.utils.JSONConvertable

data class BaseResponse(
    val success: Boolean = false,
    val message: String = "",
    val error: String = "",
    val result: Result? = null,
    val time: String = "",
    val ver: String = ""
) : JSONConvertable {
    data class Result(
        val user_info: TownsmanUsers? = null,
        val bind_users_geo_points: List<UserGeoPointInfo>? = null,
        val user_geo_history_points: List<UserGeoHistoryPoint>? = null,
        val add_offline_geo_history_result: List<OfflineGeoPointSavedInfo>? = null,
        val osrm_route_response: OSRMRouteResponse? = null,
        val mobile_app_settings: UprMobileAppSettings? = null,
        val user_private_app_settings: UsersPrivateAppSettingsInfo? = null,
        val reverse_geocoding_service_client: ReverseGeoCodingService? = null


        /*,
        val user_binding_info: UsersBindingsInfo? = null,
        val ward_credentials: WardLoginCredentials? = null,
        val bind_users_joined_info_list: List<UserJoinedInfo>? = null,
        val bind_users_list: List<UserInfo>? = null,
        val user_sos_request_info: UserSosRequestInfo? = null,
        val user_sos_request_infos: List<UserSosRequestInfo>? = null,
        val user_joined_sos_request_list: List<UserJoinedSosRequestInfo>? = null,
        val user_joined_alarm_request_list: List<UsersJoinedAlarmRequests>? = null,
        val user_joined_media_request_list: List<UsersJoinedMediaRequests>? = null,
        val guardian_users_joined_ward_list: List<GuardianUserJoinedWardInfo>? = null,
        val health_status_joined_types_list: List<HealthStatusJoinedTypes>? = null*/
    )
}