package kg.cdt.townsman.wsapi.exception

import android.app.Dialog
import android.content.Context
import kg.cdt.townsman.R
import kg.cdt.townsman.utils.LogUtils
import kg.cdt.townsman.utils.PrettyDialogUtils
import kg.cdt.townsman.utils.ThreadGuiUtils.doInPostHandler
import kg.cdt.townsman.utils.ToastUtils

class TmWsApiFun{
    fun tmWsApiRuntimeException(
        e: TmWsApiRuntimeException,
        context: Context,
        progressDialog: Dialog,
        TAG: String
    ) {
        val exMsg = e.message ?: ""
        LogUtils.info(TAG, exMsg)

        val warningMessage =
            when (exMsg) {
                TmWsApiMessages.login_or_password_is_not_correct, TmWsApiMessages.unauthorized -> context.getString(
                    R.string.logout_auth_token_failed_msg
                )
                TmWsApiMessages.login_in_session_is_empty -> context.getString(
                    R.string.necessary_login_in_again_msg
                )

                else -> "${context.getString(R.string.pending_process_failed_msg)}: $exMsg"
            }

        doInPostHandler {

            progressDialog.dismiss()

            // в зависимости от ответа сервера показываем сообщение в тоаст или в pretty dialog
            when (exMsg) {
                TmWsApiMessages.login_or_password_is_not_correct, TmWsApiMessages.unauthorized, TmWsApiMessages.login_in_session_is_empty -> {
                    ToastUtils.ToastLong(context, warningMessage)
                }
                else ->// warning dialog
                    PrettyDialogUtils().showError(
                        ctx = context,
                        title = warningMessage,
                        message = "",
                        clickHandler = Runnable {
                        }
                    )
            }
        }
    }

    fun exceptions(e: Exception, context: Context, progressDialog: Dialog, TAG: String) {
        LogUtils.error(TAG, e.message, e)

        val exMsg = e.message ?: ""
        val warningMessage =
            ExceptionDetector.exceptionDetectorRuntimeException(
                exMsg,
                context
            )
        doInPostHandler {
            progressDialog.dismiss()

            PrettyDialogUtils().showError(
                ctx = context,
                title = "${context.getString(R.string.pending_process_failed_msg)}:",
                message = warningMessage
            )
        }
    }
}