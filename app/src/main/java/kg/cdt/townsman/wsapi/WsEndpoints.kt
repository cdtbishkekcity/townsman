package kg.cdt.townsman.wsapi

import kg.cdt.townsman.debug.DebugModeGlobal

object WsEndpoints {
    const val wsHubDomain = "312.e-meria.kg"
    const val wsHubDomainDev = "192.168.190.164:8080"

    fun definedEndpoint(): String {
        return if (DebugModeGlobal.isDebugModeEnabled()) {
            "http://$wsHubDomainDev"
        } else {
            "https://$wsHubDomain"
        }
    }

    fun definedWsEndpoint(): String {
        return if (DebugModeGlobal.isDebugModeEnabled()) {
            "ws://$wsHubDomainDev"
        } else {
            "wss://$wsHubDomain"
        }
    }
}