package kg.cdt.townsman.wsapi.model

data class OfflineGeoPointSavedInfo(
    var id: Long? = null,
    var saved: Boolean = false
)