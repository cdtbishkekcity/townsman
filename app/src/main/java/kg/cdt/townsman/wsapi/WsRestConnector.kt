package kg.cdt.townsman.wsapi

import com.blankj.utilcode.util.EncryptUtils
import kg.cdt.townsman.utils.*
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.ACCESS_GRANTED
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.LOGOUT_SUCCESS
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.login_or_password_is_not_correct
import kg.cdt.townsman.wsapi.exception.TmWsApiMessages.unauthorized
import kg.cdt.townsman.wsapi.exception.TmWsApiRuntimeException
import kg.cdt.townsman.wsapi.model.*

class WsRestConnector {
    private val TAG = this.javaClass.name

    companion object {
        var wsURL: String = ""
            get() {
                if (field.isEmpty()) {
//                    field = "${WsEndpoints.definedEndpoint()}/townsman-ws/"
                    field = " http://192.168.190.106:8080/townsman-ws/"
                }
                return field
            }

        private const val commonPrivateKey = "ZuQNOpaJKBZNX1DdI6KM"

        const val loginUrlPrefix = "rest/users/login"
        const val logoutUrlPrefix = "rest/users/logout"
        const val getPersonalUrlPrefix = "rest/users/get-personal"
        const val setUserInfoUrlPrefix = "rest/users/set-user-info"
        const val setRegisteredPushTokenUrlPrefix = "rest/users/set-registered-push-token"
        const val getMobileAppSettings = "rest/users/get-mobile-app-settings"
        const val getPrivateAppSettings = "rest/users/get-private-app-settings"
        const val getAddressByGeoPoint = "rest/users/get-address-geo-point"

        private var wsRestConnector: WsRestConnector? = null

        fun getInstance(): WsRestConnector {
            wsRestConnector = wsRestConnector ?: WsRestConnector()
            return wsRestConnector!!
        }

        @Synchronized
        fun reInit() {
            wsRestConnector = WsRestConnector()
        }

        // autoLoginOnUnauthorized
        var autoLoginOnUnauthorized: RunnableUtils? = null
    }

    val gsonUtils = GsonUtils()

    var Login = ""
    var Password = ""
    var SessionHeader = ""
        set(value) {
            if (value.isNotEmpty()) {
                OkHttpUtils.setAuthSessionToken(value)
            }
            field = value
        }

    var UserId = ""
    var UserProfile = ""

    /**
     * try user login
     */
    fun tryLogin(
        login: String,
        password: String
    ): Boolean {
        try {
            // clear auth data
            clearSessionHeader()
            OkHttpUtils.removeAuthSessionHeader()

            val passDelimiter = "&&&"
            val now = DateTimeUtils().nowMs()
            val encryptHmacSHA384string = EncryptUtils.encryptHmacSHA384ToString(password, now)

            // add http basic authentication header
            OkHttpUtils.addCredentialsBasicHeader(
                login,
                encryptHmacSHA384string + passDelimiter + now
            )

            val postParam = ""
            val tryLoginResponse = OkHttpUtils()
                .tryPost("$wsURL$loginUrlPrefix", postParam, 2, 3000L)

            thrownOnAuthenticateBasicRealm()

            val tryLogin = detectResponse(tryLoginResponse.toString())

            // replace and remove auth and cookie headers
            OkHttpUtils.setCookieAfterAuthAndRemoveCredentials()

            when (tryLogin) {
                is BaseResponse ->
                    if (tryLogin.success) {
                        if (tryLogin.message.equals(ACCESS_GRANTED, true)) {
                            Login = login
                            Password = password
                            SessionHeader = OkHttpUtils.getAuthSessionToken()

                            return true
                        } else {
                            throw TmWsApiRuntimeException(tryLogin.message)
                        }
                    } else {
                        throw TmWsApiRuntimeException(tryLogin.message)
                    }
                is BaseErrorResponse ->// handler of error message
                    if (tryLogin.error.equals(unauthorized, true)) {
                        throw TmWsApiRuntimeException(tryLogin.message)
                    } else {
                        throw TmWsApiRuntimeException(tryLogin.error)
                    }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)

            throw e
        }

        return false
    }

    /**
     * try user logout by auth token
     */
    fun tryLogout(): Boolean {
        try {
            val tryLogoutResponse = OkHttpUtils()
                .tryPost("$wsURL$logoutUrlPrefix", "", 2, 3000L)

            val tryLogout = detectResponse(tryLogoutResponse.toString())

            thrownOnAuthenticateBasicRealm()

            // clear auth data
            clearSessionHeader()
            OkHttpUtils.removeAuthSessionHeader()

            when (tryLogout) {
                is BaseResponse ->
                    if (tryLogout.success) {
                        if (tryLogout.message.equals(LOGOUT_SUCCESS, true)) {
                            return true
                        } else {
                            throw TmWsApiRuntimeException(tryLogout.message)
                        }
                    } else {
                        throw TmWsApiRuntimeException(tryLogout.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    if (tryLogout.error.equals(unauthorized, true)) {
                        throw TmWsApiRuntimeException(tryLogout.message)
                    } else {
                        throw TmWsApiRuntimeException(tryLogout.error)
                    }
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }

        return false
    }

    /**
     * try get user info
     */
    fun getUserPersonal(
        autoLogin: Boolean = true
    ): BaseResponse.Result? {
        try {
            val getUserInfoResponse = OkHttpUtils()
                .tryPost("$wsURL$getPersonalUrlPrefix", "", 30, 7000L)

            val detectResponse = detectResponse(getUserInfoResponse.toString())

            thrownOnAuthenticateBasicRealm()

            if (autoLogin) {
                val runnableOnUnauthorizedError = runnableOnUnauthorizedError(detectResponse)
                if (runnableOnUnauthorizedError) {
                    return getUserPersonal(autoLogin = false)
                }
            }

            when (detectResponse) {
                is BaseResponse ->
                    if (detectResponse.success) {
                        return detectResponse.result
                    } else {
                        throw TmWsApiRuntimeException(detectResponse.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    baseErrorResponseThrower(detectResponse)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }
        return null
    }

    /**
     * try set user info
     */
    fun setUserInfo(
        pin: String,
        second_name: String,
        name: String,
        last_name: String,
        email: String,
        autoLogin: Boolean = true
    ): TownsmanUsers? {
        try {
            val setUserInfoResponse = OkHttpUtils()
                .tryPost(
                    "$wsURL$setUserInfoUrlPrefix",
                    "pin=$pin" +
                            "&second_name=$second_name" +
                            "&name=$name" +
                            "&last_name=$last_name" +
                            "&email=$email",
                    2,
                    3000L
                )

            val detectResponse = detectResponse(setUserInfoResponse.toString())

            thrownOnAuthenticateBasicRealm()

            if (autoLogin) {
                val runnableOnUnauthorizedError = runnableOnUnauthorizedError(detectResponse)
                if (runnableOnUnauthorizedError) {
                    return setUserInfo(
                        pin,
                        second_name,
                        name,
                        last_name,
                        email,
                        autoLogin = false
                    )
                }
            }

            when (detectResponse) {
                is BaseResponse ->
                    if (detectResponse.success) {
                        return detectResponse.result?.user_info
                    } else {
                        throw TmWsApiRuntimeException(detectResponse.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    baseErrorResponseThrower(detectResponse)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }
        return null
    }

    /**
     * try set registered push token
     */
    fun setRegisteredPushToken(
        registered_push_token: String,
        app_version: String,
        os_version: String,
        autoLogin: Boolean = true
    ): TownsmanUsers? {
        try {
            val setPushTokenResponse = OkHttpUtils()
                .tryPost(
                    "$wsURL$setRegisteredPushTokenUrlPrefix",
                    "registered_push_token=$registered_push_token" +
                            "&app_version=$app_version" +
                            "&os_version=$os_version",
                    2,
                    3000L
                )

            val detectResponse = detectResponse(setPushTokenResponse.toString())

            thrownOnAuthenticateBasicRealm()

            if (autoLogin) {
                val runnableOnUnauthorizedError = runnableOnUnauthorizedError(detectResponse)
                if (runnableOnUnauthorizedError) {
                    return setRegisteredPushToken(
                        registered_push_token,
                        app_version,
                        os_version,
                        autoLogin = false
                    )
                }
            }

            when (detectResponse) {
                is BaseResponse ->
                    if (detectResponse.success) {
                        return detectResponse.result?.user_info
                    } else {
                        throw TmWsApiRuntimeException(detectResponse.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    baseErrorResponseThrower(detectResponse)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }
        return null
    }

    /**
     * try get user app settings
     */
    fun getMobileAppSettings(
        autoLogin: Boolean = true
    ): UprMobileAppSettings? {
        try {
            val getAppSettings = OkHttpUtils()
                .tryPost("$wsURL$getMobileAppSettings", "", 2, 3000L)

            val detectResponse = detectResponse(getAppSettings.toString())

            thrownOnAuthenticateBasicRealm()

            if (autoLogin) {
                val runnableOnUnauthorizedError = runnableOnUnauthorizedError(detectResponse)
                if (runnableOnUnauthorizedError) {
                    return getMobileAppSettings(
                        autoLogin = false
                    )
                }
            }

            when (detectResponse) {
                is BaseResponse ->
                    if (detectResponse.success) {
                        return detectResponse.result?.mobile_app_settings
                    } else {
                        throw TmWsApiRuntimeException(detectResponse.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    baseErrorResponseThrower(detectResponse)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }
        return null
    }

    /**
     * try get user private app settings
     */
    fun getUserPrivateAppSettings(
        autoLogin: Boolean = true
    ): UsersPrivateAppSettingsInfo? {
        try {
            val getPrivateAppSettings = OkHttpUtils()
                .tryPost("$wsURL$getPrivateAppSettings", "", 2, 3000L)

            val detectResponse = detectResponse(getPrivateAppSettings.toString())

            thrownOnAuthenticateBasicRealm()

            if (autoLogin) {
                val runnableOnUnauthorizedError = runnableOnUnauthorizedError(detectResponse)
                if (runnableOnUnauthorizedError) {
                    return getUserPrivateAppSettings(
                        autoLogin = false
                    )
                }
            }

            when (detectResponse) {
                is BaseResponse ->
                    if (detectResponse.success) {
                        return detectResponse.result?.user_private_app_settings
                    } else {
                        throw TmWsApiRuntimeException(detectResponse.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    baseErrorResponseThrower(detectResponse)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }
        return null
    }

    /**
     * try get address by geo point
     */
    fun getAddressByGeoPoint(
        lat: String,
        lon: String,
        autoLogin: Boolean = true
    ): ReverseGeoCodingService? {
        try {
            val getAddressByGeoPoints = OkHttpUtils()
                .tryPost("$wsURL${getAddressByGeoPoint}/$lat/$lon", "", 2, 3000L)

            val detectResponse = detectResponse(getAddressByGeoPoints.toString())

            thrownOnAuthenticateBasicRealm()

            if (autoLogin) {
                val runnableOnUnauthorizedError = runnableOnUnauthorizedError(detectResponse)
                if (runnableOnUnauthorizedError) {
                    return getAddressByGeoPoint(
                        lat,
                        lon,
                        autoLogin = false
                    )
                }
            }

            when (detectResponse) {
                is BaseResponse ->
                    if (detectResponse.success) {
                        return detectResponse.result?.reverse_geocoding_service_client
                    } else {
                        throw TmWsApiRuntimeException(detectResponse.message)
                    }
                is BaseErrorResponse -> // handler of error message
                    baseErrorResponseThrower(detectResponse)
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
            throw e
        }
        return null
    }
    /**
     * ----------------------------
     */

    /**
     * handler of unauthorized response message
     */
    private fun runnableOnUnauthorizedError(
        responseObj: Any
    ): Boolean {
        var unauthorizedError = false

        when (responseObj) {
            is BaseResponse ->
                if (responseObj.success.not()) {
                    if (responseObj.message.equals(unauthorized, true)) {
                        unauthorizedError = true
                    } else {
                        throw TmWsApiRuntimeException(responseObj.message)
                    }
                }
            is BaseErrorResponse ->// handler of error message
                if (responseObj.message.equals(unauthorized, true)) {
                    unauthorizedError = true
                } else {
                    throw TmWsApiRuntimeException(responseObj.message)
                }
        }

        return if (unauthorizedError) {
            // try auto login
            val tryLogin = tryLogin(
                Login,
                Password
            )

            if (tryLogin) {
                // runnable handler
                autoLoginOnUnauthorized?.run()
            }

            tryLogin
        } else {
            false
        }
    }

    private fun baseErrorResponseThrower(detectResponse: BaseErrorResponse) {
        if (detectResponse.error.equals(unauthorized, true)) {
            throw TmWsApiRuntimeException(detectResponse.message)
        } else {
            throw TmWsApiRuntimeException(detectResponse.error)
        }
    }

    private fun detectResponse(response: String): JSONConvertable {
        return response.toObject<BaseResponse>()
            ?: response.toObject<BaseErrorResponse>()
            ?: BaseResponse()
    }

    private fun clearSessionHeader() {
        SessionHeader = String()
    }

    private fun thrownOnAuthenticateBasicRealm() {
        // detected request on WWW-Authenticate: Basic realm="Realm"
        if (OkHttpUtils.DetectedAuthenticateBasicRealm) {
            throw TmWsApiRuntimeException(login_or_password_is_not_correct)
        }
    }
}