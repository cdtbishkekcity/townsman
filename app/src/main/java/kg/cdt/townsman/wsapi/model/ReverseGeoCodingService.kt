package kg.cdt.townsman.wsapi.model

data class ReverseGeoCodingService(
    val place_id: Int? = null,
    val licence: String? = null,
    val osm_type: String? = null,
    val osm_id: Int? = null,
    val lat: String? = null,
    val lon: String? = null,
    val display_name: String? = null,
    val address: Address? = null,
    val boundingbox: List<String>? = null
)