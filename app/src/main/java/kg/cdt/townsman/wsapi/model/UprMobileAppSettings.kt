package kg.cdt.townsman.wsapi.model

data class UprMobileAppSettings(
    var id: Long = 0L,
    var geo_min_distance: Long = 30L,
    var geo_min_time: Long = 30L,
    var geo_timer_second: Long = 60L,
    var geo_objects_in_radius: Long = 60L,
    var battery_timer: Long = 90L,
    var use_geo_wifi: Boolean = false,
    var offline_geo_timer_second: Long = 300L,
    var block_pass_failed_count: Long = 9L,
    var block_pass_failed_sec: Long = 180L,
    var block_sync_code_failed_sec: Long = 180L,
    var update_map_sec: Long = 30L,
    var stream_server_endpoint: String = "https://media-streaming.meria.kg"
)