package kg.cdt.townsman.wsapi.model

enum class UserLanguages {
    RU,
    KY
}