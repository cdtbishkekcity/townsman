package kg.cdt.townsman.wsapi.model

data class UsersPrivateAppSettingsInfo(
    var file_upload_api_url: String = "",
    var file_upload_api_key: String = ""
)