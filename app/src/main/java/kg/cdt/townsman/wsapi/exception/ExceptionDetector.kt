package kg.cdt.townsman.wsapi.exception

import android.content.Context
import kg.cdt.townsman.R

object ExceptionDetector {
    const val no_address_associated_with_hostname = "no address associated with hostname"
    private const val failed_to_connect = "failed to connect"
    private const val socket_timeout_exception = "SocketTimeoutException"
    private const val ssl_handshake_exception = "SSLHandshakeException"
    const val timeout = "timeout"

    fun exceptionDetectorRuntimeException(exception: String, context: Context): String {
        return when {
            exception.contains(no_address_associated_with_hostname, true) ||
                    exception.contains(failed_to_connect, true)  ||
                    exception.contains(socket_timeout_exception, true) ||
                    exception.contains(ssl_handshake_exception, true) ||
                    exception.contains(timeout, true) -> context.getString(R.string.internet_connection_problems_msg)
            else -> "${context.getString(R.string.pending_process_failed_msg)}: $exception"
        }
    }
}