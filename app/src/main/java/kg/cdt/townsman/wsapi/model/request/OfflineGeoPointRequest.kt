package kg.cdt.townsman.wsapi.model.request

data class OfflineGeoPointRequest(
    var id: Long? = null,
    var user_id: Long? = null,
    var lat: String? = null,
    var lon: String? = null,
    var time: String? = null
)