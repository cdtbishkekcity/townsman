package kg.cdt.townsman.wsapi.model

import kg.cdt.townsman.utils.JSONConvertable

data class BaseErrorResponse(
    val timestamp: String = "",
    val status: Int = 0,
    val error: String = "",
    val message: String = "",
    val path: String = ""
) : JSONConvertable
/*
{
    "timestamp": "2019-11-26 15:50:52",
    "status": 200,
    "error": "OK",
    "message": "Unauthorized",
    "path": "/control-works-ws/403"
}
*/