package kg.cdt.townsman.geo.osm

import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint

class OsmConst {
    companion object {
        val GeoDefaultBishkek = GeoPoint(42.876143, 74.586440)

        const val ZoomDefault = 16.5

        // tile map default sources
        val tileSourceDefault = TileSourceFactory.HIKEBIKEMAP
        val tileSourceDefault1 = TileSourceFactory.HIKEBIKEMAP
        val tileSourceDefault2 = XYTileSource(
            "Cem103TileSource",
            0,
            18,
            256,
            ".png",
            arrayOf("http://host-tile-server:8080/tile/"),
            "© OpenStreetMap contributors"
        )
    }
}