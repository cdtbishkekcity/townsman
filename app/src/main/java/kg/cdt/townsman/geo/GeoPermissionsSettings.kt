package kg.cdt.townsman.geo

import android.Manifest
import android.content.Context
import android.view.Gravity
import com.blankj.utilcode.constant.PermissionConstants
import com.blankj.utilcode.util.PermissionUtils
import kg.cdt.townsman.R
import kg.cdt.townsman.utils.LogUtils
import kg.cdt.townsman.utils.RunnableUtils
import libs.mjn.prettydialog.PrettyDialog

class GeoPermissionsSettings {
    private val TAG = this.javaClass.name

    companion object {
        var geoIsGranted = false
        var dialogIsRunning = false
    }

    var onGranted: RunnableUtils? = null

    fun requestPermission(context: Context): GeoPermissionsSettings {
        try {
            if (GeoGpsConnector.geoPermissionIsDenied(context) && !dialogIsRunning) {
                val pDialog = PrettyDialog(context)
                pDialog
                    .setTitleColor(R.color.pdlg_color_red)
                    .setMessage(context.getString(R.string.geo_service_disabled_dlg))
                    .setMessageColor(R.color.pdlg_color_red)
                    .setIcon(R.drawable.pdlg_icon_info)
                    .setIconTint(R.color.pdlg_color_red)
                    .addButton(
                        context.getString(R.string.next_comm),
                        R.color.pdlg_color_black,
                        android.R.color.white
                    ) {
                        try {
                            PermissionUtils.permission(PermissionConstants.LOCATION).apply {
                                callback(object : PermissionUtils.FullCallback {
                                    override fun onGranted(granted: MutableList<String>) {
                                        println(granted)
                                        LogUtils.info(TAG, "permissionsGranted")

                                        geoIsGranted = true

                                        onGranted?.run(granted)
                                    }

                                    override fun onDenied(
                                        deniedForever: MutableList<String>,
                                        denied: MutableList<String>
                                    ) {
                                        println(deniedForever)
                                        println(denied)
                                        LogUtils.info(TAG, "permissionsDenied")

                                        geoIsGranted = false
                                    }
                                })

                                request()
                            }
                        } catch (e: Exception) {
                            LogUtils.error(TAG, e.message)
                        }

                        pDialog.dismiss()

                        dialogIsRunning = false
                    }
                    .setGravity(Gravity.CENTER)
                pDialog.setOnCancelListener {
                    dialogIsRunning = false
                }
                pDialog.setCancelable(false)
                pDialog.show()

                dialogIsRunning = true
            } else {
                geoIsGranted = true

                LogUtils.info(TAG, "${Manifest.permission.ACCESS_FINE_LOCATION}: GRANTED")
            }

        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }

        return this
    }

    fun requestSystemSettings(context: Context): GeoPermissionsSettings {
        try {
            if (!dialogIsRunning) {
                val pDialog = PrettyDialog(context)
                pDialog
                    .setTitleColor(R.color.pdlg_color_red)
                    .setMessage(context.getString(R.string.geo_service_disabled_dlg))
                    .setMessageColor(R.color.pdlg_color_red)
                    .setIcon(R.drawable.pdlg_icon_info)
                    .setIconTint(R.color.pdlg_color_red)
                    .addButton(
                        context.getString(R.string.run_geo_settings),
                        R.color.pdlg_color_black,
                        android.R.color.white
                    ) {
                        GeoGpsConnector.callGeoSystemSettings(context)

                        pDialog.dismiss()

                        dialogIsRunning = false
                    }
                    .setGravity(Gravity.CENTER)
                pDialog.setOnCancelListener {
                    dialogIsRunning = false
                }
                pDialog.setCancelable(false)
                pDialog.show()

                dialogIsRunning = true
            }
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message, e)
        }

        return this
    }
}