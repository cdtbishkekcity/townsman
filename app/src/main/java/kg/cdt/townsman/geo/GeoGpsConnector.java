package kg.cdt.townsman.geo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kg.cdt.townsman.utils.RunnableUtils;


public class GeoGpsConnector {
    private static final String TAG = GeoGpsConnector.class.getName();

    private static GeoGpsConnector geoInstance;
    private static Context context = null;

    public static float MIN_DISTANCE_UPDATES = 10F;
    public static long MIN_TIME_UPDATES = 30 * 1000L;

    private LocationManager locationManager;
    private GpsStatus GPStatus;

    private Location lastLocation;
    private double lastLatitude;
    private double lastLongitude;
    private double networkLatitude;
    private double networkLongitude;

    private boolean isInitGps = false;

    public final double RoundAfterPoint = 1000000;

    public static RunnableUtils onGpsLocationChanged1;
    public static RunnableUtils onGpsLocationChanged2;
    public static RunnableUtils onProviderDisabled;
    public static RunnableUtils onProviderEnabled;
    public static RunnableUtils onStatusChanged;
    public static RunnableUtils onGpsStatusChanged;

    public static RunnableUtils onGeoPermissionFailed;

    protected interface IBaseGpsLocationListener extends LocationListener, GpsStatus.Listener {
        void onLocationChanged(Location location);

        void onProviderDisabled(String provider);

        void onProviderEnabled(String provider);

        void onStatusChanged(String provider, int status, Bundle extras);

        void onGpsStatusChanged(int event);
    }

    protected IBaseGpsLocationListener locationListener = new IBaseGpsLocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                    lastLocation = location;
                    lastLatitude = (double) Math.round(location.getLatitude() * RoundAfterPoint) / RoundAfterPoint;
                    lastLongitude = (double) Math.round(location.getLongitude() * RoundAfterPoint) / RoundAfterPoint;
                }
            }

            if (onGpsLocationChanged1 != null) {
                onGpsLocationChanged1.run(location);
            }
            if (onGpsLocationChanged2 != null) {
                onGpsLocationChanged2.run(location);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (onProviderDisabled != null) {
                onProviderDisabled.run(provider);
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (onProviderEnabled != null) {
                onProviderEnabled.run(provider);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            String gps_state = "";
            switch (status) {
                case LocationProvider.AVAILABLE:
                    gps_state = "AVAILABLE";
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    gps_state = "OUT_OF_SERVICE";
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    gps_state = "TEMPORARILY_UNAVAILABLE";
                    break;
            }

            if (onStatusChanged != null) {
                onStatusChanged.run(String.format("%s=%s", provider, gps_state));
            }
        }

        @Override
        public void onGpsStatusChanged(int event) {
            String GPStatus = "";
            GeoGpsConnector.this.GPStatus = locationManager.getGpsStatus(GeoGpsConnector.this.GPStatus);
            switch (event) {
                case GpsStatus.GPS_EVENT_STARTED:
                    GPStatus = "GPS_EVENT_STARTED";
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    GPStatus = "GPS_EVENT_STOPPED";
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    GPStatus = "GPS_EVENT_FIRST_FIX";
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    GPStatus = "GPS_EVENT_SATELLITE_STATUS";
                    break;
            }

            if (onGpsStatusChanged != null) {
                onGpsStatusChanged.run(GPStatus);
            }
        }
    };

    public static synchronized GeoGpsConnector getInstance() {
        if (geoInstance == null) {
            geoInstance = new GeoGpsConnector();
        }
        return geoInstance;
    }

    public static synchronized void reInit() {
        if (geoInstance != null && geoInstance.isEnabledGps()) {
            geoInstance.RemoveLocationListener();
        }
        geoInstance = null;
        geoInstance = new GeoGpsConnector();
    }

    @SuppressLint("MissingPermission")
    public GeoGpsConnector() {
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (geoPermissionIsDenied(getContext())) {
            if (onGeoPermissionFailed != null) {
                onGeoPermissionFailed.run();

                return;
            } else {
                throw new IllegalArgumentException("no_granted_permissions");
            }
        }

        // KitKat checking
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_UPDATES,
                        MIN_DISTANCE_UPDATES,
                        locationListener);

                isInitGps = true;
                isProviderExistsAndEnabled_API19 = true;
            } else {
                isInitGps = false;
                isProviderExistsAndEnabled_API19 = false;

                throw new IllegalArgumentException("gps_provider_not_exists_or_not_enabled");
            }
            return;
        }

        lastLatitude = 0.0D;
        lastLongitude = 0.0D;

        // main geo initializer
        if (locationManager != null) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_UPDATES,
                    MIN_DISTANCE_UPDATES,
                    locationListener);

            isInitGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MIN_TIME_UPDATES,
                    MIN_DISTANCE_UPDATES,
                    locationListener
            );
        }
    }

    public synchronized void RemoveLocationListener() {
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);

            isInitGps = false;
            geoInstance = null;
        }
    }

    @SuppressLint("MissingPermission")
    public Location getLastKnownPosition() {
        Location location = new Location(LocationManager.GPS_PROVIDER);
//        location.setLatitude(OsmConst.Companion.getGeoDefaultBishkek().getLatitude());
//        location.setLongitude(OsmConst.Companion.getGeoDefaultBishkek().getLongitude());

        if (locationManager != null) {
            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocation != null) {
                return lastKnownLocation;
            } else {
                return location;
            }
        }
        return location;
    }

    public static boolean geoPermissionIsDenied(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    public void stop() {
        RemoveLocationListener();
    }

    public static Context getContext() {
        if (context == null) {
            throw new IllegalArgumentException("context is not initialized");
        }
        return context;
    }

    public static void setContext(Context ctx) {
        if (ctx == null) {
            throw new IllegalArgumentException("context is not initialized");
        }
        context = ctx;
    }

    public synchronized Location getLastLocation() {
        return lastLocation;
    }

    public synchronized double getLastLatitude() {
        return lastLatitude;
    }

    public synchronized double getLastLongitude() {
        return lastLongitude;
    }

    public synchronized double getNetworkLatitude() {
        return networkLatitude;
    }

    public synchronized double getNetworkLongitude() {
        return networkLongitude;
    }

    public synchronized boolean isEnabledGps() {
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public synchronized boolean isEnabledNetwork() {
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public boolean isInitGps() {
        return isInitGps;
    }

    public synchronized static boolean isProviderExistsAndEnabled_API19() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            return isProviderExistsAndEnabled_API19;
        } else {
            return true;
        }
    }

    public static boolean isEnabledGps(Activity activity) {
        LocationManager service = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private static boolean isProviderExistsAndEnabled_API19 = false;

    public static void callGeoSystemSettings(Context activity) {
        activity.startActivity(
                new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public static void showDialogYesNoWithGPSettings(final Activity activity) {
        /*DialogUtils.showDialogYesNo(activity, activity.getString(R.string.gps_services_need_enable),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GeoGpsConnector.callGeoSystemSettingsIntent(activity);
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });*/
    }

    /**
     * Determine if point in polygon
     *
     * @param location current location point
     * @param polygon  polygon list points
     * @return TRUE or FALSE
     */
    /*public static boolean insidePointInPolygon(Location location, List<LatLng> polygon) {
        return PolyUtil.containsLocation(new LatLng(location.getLatitude(), location.getLongitude()), polygon, true);
    }*/


    /**
     * method2: Determine if point in polygon
     *
     * @param p
     * @param polygon
     * @return TRUE or FALSE
     */
    public static boolean PointIsInPolygon(Location p, List<Location> polygon) {
        boolean isInside = false;
        double minX = polygon.get(0).getLongitude(), maxX = polygon.get(0).getLongitude();
        double minY = polygon.get(0).getLatitude(), maxY = polygon.get(0).getLatitude();
        for (int n = 1; n < polygon.size(); n++) {
            Location q = polygon.get(n);
            minX = Math.min(q.getLongitude(), minX);
            maxX = Math.max(q.getLongitude(), maxX);
            minY = Math.min(q.getLatitude(), minY);
            maxY = Math.max(q.getLatitude(), maxY);
        }

        if (p.getLongitude() < minX || p.getLongitude() > maxX || p.getLatitude() < minY || p.getLatitude() > maxY) {
            return false;
        }

        for (int i = 0, j = polygon.size() - 1; i < polygon.size(); j = i++) {
            if ((polygon.get(i).getLatitude() > p.getLatitude()) != (polygon.get(j).getLatitude() > p.getLatitude())
                    && p.getLongitude() < (polygon.get(j).getLongitude() - polygon.get(i).getLongitude()) * (p.getLatitude() - polygon.get(i).getLatitude()) / (polygon.get(j).getLatitude() - polygon.get(i).getLatitude()) + polygon.get(i).getLongitude()) {
                isInside = !isInside;
            }
        }

        return isInside;
    }


    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public static float DistanceCalcFrom(Location location1, Location location2) {
        return DistanceCalcFrom((float) location1.getLatitude(), (float) location1.getLongitude(), (float) location2.getLatitude(), (float) location2.getLongitude());
    }

    @SuppressLint("DefaultLocale")
    private String toStringLocation(Location location) {
        if (location == null) {
            return "";
        }
        return String.format(
                "Coordinates: lat = %1$.4f, lon = %2$.4f, time = %3$tF %3$tT",
                location.getLatitude(),
                location.getLongitude(),
                new Date(location.getTime()));
    }

    public static float DistanceCalcFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }


    public static class Geocode {
        private float latitude;
        private float longitude;

        public Geocode() {
        }

        public Geocode(float latitude, float longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public float getLatitude() {
            return latitude;
        }

        public void setLatitude(float latitude) {
            this.latitude = latitude;
        }

        public float getLongitude() {
            return longitude;
        }

        public void setLongitude(float longitude) {
            this.longitude = longitude;
        }
    }

    public static class GeoPolygon {
        private ArrayList<Geocode> points;

        public GeoPolygon() {
            this.points = new ArrayList<Geocode>();
        }

        public GeoPolygon(ArrayList<Geocode> points) {
            this.points = points;
        }

        public GeoPolygon add(Geocode geo) {
            points.add(geo);
            return this;
        }

        public boolean inside(Geocode geo) {
            int i, j;
            boolean c = false;
            for (i = 0, j = points.size() - 1; i < points.size(); j = i++) {
                if (((points.get(i).getLongitude() > geo.getLongitude()) != (points.get(j).getLongitude() > geo.getLongitude())) &&
                        (geo.getLatitude() < (points.get(j).getLatitude() - points.get(i).getLatitude()) * (geo.getLongitude() - points.get(i).getLongitude()) / (points.get(j).getLongitude() - points.get(i).getLongitude()) + points.get(i).getLatitude()))
                    c = !c;
            }
            return c;
        }
    }
}
