package kg.cdt.townsman.geo.osm

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import kg.cdt.townsman.wsapi.model.OSRMRouteResponse
import kg.cdt.townsman.R
import kg.cdt.townsman.utils.LogUtils
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polygon
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import kotlin.math.roundToInt

class OsmHelper {
    companion object {
        private val TAG = this.javaClass.name
        fun setGeoAnimate2OSM(
            mapView: MapView,
            geoPoint: GeoPoint,
            zoom: Boolean = true,
            zoomCount: Double = OsmConst.ZoomDefault
        ) = try {
            mapView.controller.run {
                animateTo(geoPoint)
                if (zoom) {
                    setZoom(zoomCount)
                }
                //setCenter(geoPoint)
            }
        } catch (e: Exception) {
        }

        fun setMarker2OSM(
            context: Context,
            mapView: MapView,
            geoPoint: GeoPoint,
            id: Int,
            time: String
        ) {
            try {
                val marker = Marker(mapView).apply {
                    position = geoPoint
                    setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
//                    icon = BitmapDrawable(createDrawableFromView(context, id)) view
                    icon = ResourcesCompat.getDrawable(context.resources, id, null)
                    title = time
                }
                mapView.overlays.clear()
                mapView.overlays.add(marker)
            } catch (e: Exception) {
            }
        }

        fun addMarker2OSM(
            context: Context,
            mapView: MapView,
            geoPoint: GeoPoint,
            id: Int,
            text: String
        ) {
            try {
                val marker = Marker(mapView).apply {
                    position = geoPoint
                    setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                    icon = ResourcesCompat.getDrawable(context.resources, id, null)
//                    setInfoWindow(null)
                    title = text
                    infoWindow

                }
                mapView.overlays.add(marker)
            } catch (e: Exception) {
            }
        }

        fun addCustomMarker2OSM(
            context: Context,
            mapView: MapView,
            geoPoint: GeoPoint,
            id: View
        ) {
            try {
                val marker = Marker(mapView).apply {
                    position = geoPoint
                    setAnchor(Marker.ANCHOR_LEFT, Marker.ANCHOR_LEFT)
                    icon = BitmapDrawable(createDrawableFromView(context, id))
                    setInfoWindow(null)

                }

                mapView.overlays.add(marker)
            } catch (e: Exception) {
            }

        }

        private fun createDrawableFromView(context: Context, view: View): Bitmap {
            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            view.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            view.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(
                view.measuredWidth,
                view.measuredHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            view.draw(canvas)

            return bitmap
        }

        fun addMarker(
            context: Context,
            mapView: MapView,
            geoPoint: GeoPoint,
            id: Int,
            time: String
        ) {
            try {
                val marker = Marker(mapView).apply {
                    position = geoPoint
                    setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER)
                    icon = ResourcesCompat.getDrawable(context.resources, id, null)
                    title = time
                }
                mapView.overlays.add(marker)
            } catch (e: Exception) {
            }
        }

        fun clearMarkers2OSM(mapView: MapView) {
            mapView.overlays.clear()
        }

        fun drawLine(
            mapView: MapView,
            context: Context,
            points: Array<GeoPoint>
        ) {
            val line = Polyline(mapView).apply {
                color = ContextCompat.getColor(context, R.color.pdlg_color_blue)
                setPoints(points.toMutableList())
            }
            line.setOnClickListener(Polyline.OnClickListener { _polyline, mapView, eventPos ->
                false
            })
            mapView.overlayManager.add(line)
        }

        fun setCompass2OSM(
            context: Context,
            mapView: MapView
        ): CompassOverlay? {
            try {
                val mCompassOverlay = CompassOverlay(
                    context,
                    InternalCompassOrientationProvider(context),
                    mapView
                )
                mCompassOverlay.enableCompass()

                mapView.overlays.add(mCompassOverlay)

                return mCompassOverlay
            } catch (e: Exception) {
            }
            return null
        }

        fun setZoomToBoundingBox(
            mapView: MapView,
            geoPoints: Array<GeoPoint>,
            padding: Float = 0.005f
        ) {
            try {
                mapView.zoomToBoundingBox(
                    findBoundingBoxForGivenLocations(geoPoints, padding),
                    //BoundingBox.fromGeoPoints(geoPoints.toMutableList()),
                    true
                )
            } catch (e: Exception) {
            }
        }

        fun findBoundingBoxForGivenLocations(
            coordinates: Array<GeoPoint>,
            padding: Float = 0.002f
        ): BoundingBox {
            var west = 0.0
            var east = 0.0
            var north = 0.0
            var south = 0.0

            for (lc in 0 until coordinates.size) {
                val loc = coordinates[lc]
                if (lc == 0) {
                    north = loc.latitude
                    south = loc.latitude
                    west = loc.longitude
                    east = loc.longitude
                } else {
                    if (loc.latitude > north) {
                        north = loc.latitude
                    } else if (loc.latitude < south) {
                        south = loc.latitude
                    }
                    if (loc.longitude < west) {
                        west = loc.longitude
                    } else if (loc.longitude > east) {
                        east = loc.longitude
                    }
                }
            }
            // OPTIONAL - Add some extra "padding" for better map display
            //val padding = 0.01
            north += padding
            south -= padding
            west -= padding
            east += padding

            return BoundingBox(north, east, south, west)
        }

        fun setMinusZoom2OSM(
            mapView: MapView,
            minusZoom: Double = 0.7
        ) {
            try {
                // minus zoom for padding from map border
                val minusZ = mapView.zoomLevelDouble - minusZoom
                mapView.controller.run {
                    setZoom(minusZ)
                }
            } catch (e: Exception) {
            }
        }

        fun setZoom2OSM(
            mapView: MapView,
            zoom: Double
        ) {
            try {
                mapView.controller.run {
                    setZoom(zoom)
                }
            } catch (e: Exception) {
            }
        }

        fun drawRoutePath(
            routeOSRM: OSRMRouteResponse,
            mapView: MapView,
            context: Context
        ) {
            try {
                if (routeOSRM.route.isEmpty()) {
                    return
                }

                val distance = routeOSRM.distance?.toFloat()?.div(1000)?.roundToInt().toString()
                val duration = routeOSRM.duration?.toFloat()?.div(60)?.roundToInt().toString()
                val description = "${context.getString(
                    R.string.route2patient_distance_msg,
                    distance
                )}, ${context.getString(
                    R.string.route2patient_duration_msg,
                    duration
                )}"

                val line = Polyline(mapView)
                line.title = context.getString(R.string.route2patient_title)
                line.subDescription = description
                line.width = 16f
                line.color = ContextCompat.getColor(context, R.color.pdlg_color_blue)

                val pts = ArrayList<GeoPoint>()
                for (listGeo in routeOSRM.route) {
                    pts.add(
                        GeoPoint(
                            listGeo[1].toDouble(),
                            listGeo[0].toDouble()
                        )
                    )
                }

                // create a polyline
                line.setPoints(pts)


                mapView.overlayManager.add(line)

            } catch (e: Exception) {
                LogUtils.error("", e.message, e)
            }
        }

        fun drawCircle(
            mapView: MapView,
            radius: Double,
            context: Context,
            geoPoint: GeoPoint
        ) {
            val circle = Polygon.pointsAsCircle(geoPoint, radius)
            val p = Polygon(mapView)
            val paint = Paint()

            paint.color = ContextCompat.getColor(context, R.color.pdlg_color_blue)
            paint.alpha = 80
            paint.style = Paint.Style.FILL
            paint.isAntiAlias = true
            p.strokeWidth = 0.0f
            p.strokeColor = ContextCompat.getColor(context, R.color.pdlg_color_blue)
            p.fillPaint.set(paint)
            p.points = circle
            p.infoWindow = null
            mapView.overlayManager.add(p)
            mapView.invalidate()

        }

        fun rotateMapByComapss(
            mapView: MapView,
            rotate: Float
        ) {
            try {
                mapView.run {
                    rotation = rotate
                }
            } catch (e: Exception) {
            }
        }
    }
}