package kg.cdt.townsman.geo

import kg.cdt.townsman.utils.LogUtils
import kg.cdt.townsman.utils.RunnableUtils
import kg.cdt.townsman.utils.TimerUtils
import java.util.*

class GeoGpsSenderTimers {
    private val TAG = this.javaClass.name

    companion object {
        var geoGpsTimerSender: GeoGpsSenderTimers? = null

        fun getInstance(): GeoGpsSenderTimers {
            geoGpsTimerSender = geoGpsTimerSender ?: GeoGpsSenderTimers()
            return geoGpsTimerSender!!
        }
    }

    var gpsTimer1: Timer? = null
    var gpsTimer2: Timer? = null
    var gpsTimer3: Timer? = null

    private var timer1CallBack: RunnableUtils? = null
    private var timer2CallBack: RunnableUtils? = null
    private var timer3CallBack: RunnableUtils? = null

    var sendGeo2LocationModule = true
    var sendGeo2LocationHistory = true

    var doctorIsGeoSender = false

    fun runGpsTimer1(period: Int = 30 * 1000, delay: Int = 3 * 1000): GeoGpsSenderTimers {
        if (gpsTimer1 == null) {
            gpsTimer1 = TimerUtils.doInTimerScheduleWithDelayInstance({
                LogUtils.info(TAG, "timer1->run")

                timer1CallBack?.run()
            }, delay, period)
        }
        return getInstance()
    }

    fun setTimer1CallBack(timerCallBack: RunnableUtils?): GeoGpsSenderTimers {
        timer1CallBack = timerCallBack
        return getInstance()
    }

    fun stopTimer1() {
        gpsTimer1?.cancel()
        gpsTimer1 = null

        LogUtils.info(TAG, "timer1->stopped")
    }

    fun runGpsTimer2(period: Int = 30 * 1000, delay: Int = 3 * 1000): GeoGpsSenderTimers {
        if (gpsTimer2 == null) {
            gpsTimer2 = TimerUtils.doInTimerScheduleWithDelayInstance({
                LogUtils.info(TAG, "timer2->run")

                timer2CallBack?.run()
            }, delay, period)
        }
        return getInstance()
    }

    fun setTimer2CallBack(timerCallBack: RunnableUtils?): GeoGpsSenderTimers {
        timer2CallBack = timerCallBack
        return getInstance()
    }

    fun stopTimer2() {
        gpsTimer2?.cancel()
        gpsTimer2 = null

        LogUtils.info(TAG, "timer2->stopped")
    }

    fun runGpsTimer3(period: Int = 7 * 1000, delay: Int = 1 * 1000): GeoGpsSenderTimers {
        if (gpsTimer3 == null) {
            gpsTimer3 = TimerUtils.doInTimerScheduleWithDelayInstance({
                LogUtils.info(TAG, "timer3->run")

                timer3CallBack?.run()
            }, delay, period)
        }
        return getInstance()
    }

    fun setTimer3CallBack(timerCallBack: RunnableUtils?): GeoGpsSenderTimers {
        timer3CallBack = timerCallBack
        return getInstance()
    }

    fun stopTimer3() {
        gpsTimer3?.cancel()
        gpsTimer3 = null

        LogUtils.info(TAG, "timer3->stopped")
    }
}