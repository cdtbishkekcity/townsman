package kg.cdt.townsman.debug

import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.utils.LogUtils

object DebugModeGlobal {
    private val TAG = this.javaClass.name

    private var isDebug = false

    private const val DEBUG_MODE = "debug_mode"
    const val DEBUG_ENABLED = "1"
    const val DEBUG_DISABLED = "0"

    private var isLoaded = false

    fun isDebugModeEnabled(): Boolean {
        try {
            if (!isLoaded) {
                isDebug = Prefs.getString(DEBUG_MODE, DEBUG_DISABLED) == DEBUG_ENABLED

                isLoaded = true
            }

            return isDebug
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
        return false
    }

    fun setSaveDebugMode(debugMode: String) {
        try {
            isDebug = debugMode == DEBUG_ENABLED
            Prefs.putString(DEBUG_MODE, debugMode)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
    }

    fun getSavedDebugMode(): String {
        try {
            return Prefs.getString(DEBUG_MODE, DEBUG_DISABLED)
        } catch (e: Exception) {
            LogUtils.error(TAG, e.message)
        }
        return String()
    }

    fun reset() {
        isDebug = false
        isLoaded = false
    }
}