package kg.cdt.townsman.firebase_services.model

import kg.cdt.townsman.wsapi.model.BaseResponse


data class FireBaseRequest(
    val data: Data? = null,
    val to: String = ""
) {
    data class Data(
        val title: String = "",
        val message: String = "",
        val fs_push_json: FsPushJson? = null
    ) {
        data class FsPushJson(
            val cmd: FsPushCmdEnum,
            val params: MutableMap<ParamEnum, String>? = null,
            val user_id: Long = 0L,
            val ver: String = BaseResponse().ver
        )

        enum class ParamEnum {
            test_firebase_service
        }

        enum class FsPushCmdEnum {
            app_new_ver_available,
            test_firebase_service
        }
    }
}