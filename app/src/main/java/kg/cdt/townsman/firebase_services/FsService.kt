package kg.cdt.townsman.firebase_services

import android.content.Intent
import com.blankj.utilcode.util.AppUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pixplicity.easyprefs.library.Prefs
import kg.cdt.townsman.MainActivity
import kg.cdt.townsman.R
import kg.cdt.townsman.firebase_services.model.FireBaseRequest
import kg.cdt.townsman.shared_prefs.User
import kg.cdt.townsman.utils.*
import kg.cdt.townsman.wsapi.WsRestConnector
import kg.cdt.townsman.wsapi.model.UserLanguages
import java.util.*

class FsService : FirebaseMessagingService() {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    private var intent: Intent? = null

    enum class FireBaseFsJson {
        fs_push_json
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        LogUtils.info(TAG, "PUSH MESSAGE RECEIVED: ${p0.data}")

        /* val powerManager = application.getSystemService(POWER_SERVICE) as PowerManager

         val mWakeLock: PowerManager.WakeLock = powerManager.newWakeLock(
             PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or
                     PowerManager.ON_AFTER_RELEASE, TAG
         )
         mWakeLock.acquire(10000)

         val wlCpu: PowerManager.WakeLock = powerManager.newWakeLock(
             PowerManager.PARTIAL_WAKE_LOCK,
             TAG
         )
         wlCpu.acquire(10000)*/

        StorageUtils().init(this, AppUtils.getAppPackageName())
        // rest session get from storage
        WsRestConnector.getInstance().SessionHeader = StorageUtils().getSessionHeader()
        val supervisorStorage = StorageUtils().getUserData().toObject<User>()
        WsRestConnector.getInstance().Login = supervisorStorage?.login.toString()
        WsRestConnector.getInstance().Password = supervisorStorage?.password.toString()

        // re save session if auto login
        WsRestConnector.autoLoginOnUnauthorized = object : RunnableUtils {
            override fun run(obj: Any?) {
            }

            override fun run() {
                StorageUtils().saveSessionHeader(WsRestConnector.getInstance().SessionHeader)
            }
        }

        if (p0.data[FireBaseFsJson.fs_push_json.toString()] != null) {
            val fsPushJson = gsonUtils.fromJson(
                p0.data[FireBaseFsJson.fs_push_json.toString()].toString(),
                FireBaseRequest.Data.FsPushJson::class.java
            )

            val title = p0.data["title"].toString()
            val message = p0.data["message"].toString()

            val titleDescription =
                when (Prefs.getString(StorageUtils.LANGUAGE, "")) {
                    UserLanguages.KY.toString() ->
                        when (fsPushJson.cmd) {
/*                            FireBaseRequest.Data.FsPushCmdEnum.created_upr_new_task -> {
                                getString(R.string.new_task)
                            }*/
                            else -> {
                                ""
                            }
                        }
                    UserLanguages.RU.toString() ->
                        when (fsPushJson.cmd) {
/*                            FireBaseRequest.Data.FsPushCmdEnum.created_upr_new_task -> {
                                getString(R.string.new_task)
                            }*/
                            else -> {
                                ""
                            }
                        }
                    else -> ""
                }

            val fsPushJsParams =
                fsPushJson.params ?: EnumMap(FireBaseRequest.Data.ParamEnum::class.java)

            when (fsPushJson.cmd) {
                FireBaseRequest.Data.FsPushCmdEnum.test_firebase_service -> {

                    intent = Intent(this, MainActivity::class.java)

                    NotificationHelper().fsNotification(
                        this,
                        "$title $titleDescription",
                        message,
                        intent
                    )
                }
                FireBaseRequest.Data.FsPushCmdEnum.app_new_ver_available -> {
                    intent = Intent(this, MainActivity::class.java)
                    intent?.putExtra(FireBaseRequest.Data.FsPushCmdEnum.app_new_ver_available.toString(), true)

                    NotificationHelper().fsNotification(
                        this,
                        getString(R.string.app_name),
                        getString(R.string.app_new_ver_available),
                        intent
                    )
                }
            }
        } else {
            intent = Intent(this, MainActivity::class.java)
            NotificationHelper().fsNotification(
                this,
                p0.data["title"].toString(),
                p0.data["message"].toString(),
                intent
            )
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

        if (StorageUtils().getPushToken().isEmpty()) {
            StorageUtils().savePushToken(p0)
        }

        LogUtils.info("Token", ": $p0")
    }
}
