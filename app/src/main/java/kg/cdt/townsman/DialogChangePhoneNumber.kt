package kg.cdt.townsman

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import kg.cdt.townsman.shared_prefs.User
import kg.cdt.townsman.utils.*
import kotlinx.android.synthetic.main.activity_login.*

class DialogChangePhoneNumber : DialogFragment() {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    private lateinit var ctx: Context

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(activity)
        val layout = inflater.inflate(R.layout.dialog_change_phone_number, null)
        val save = layout.findViewById<View>(R.id.save_button)
        val cancel = layout.findViewById<View>(R.id.cancel_button)

        val currentPhoneNumber = layout.findViewById<View>(R.id.current_phone_number) as TextView
        val newPhoneNumber = layout.findViewById<View>(R.id.new_phone_number) as TextView

        ctx = context!!

        val builder = AlertDialog.Builder(activity)
        builder.setView(layout)
        val alert = builder.create() as AlertDialog

        val getUserInfo = StorageUtils().getUserData().toObject<User>()
        currentPhoneNumber.text = getUserInfo?.login

        //add mask phone
        PhoneNumberHelper().phoneMask(newPhoneNumber, PhoneNumberHelper().phoneMaskFormat2)
        //show 996
        newPhoneNumber.text = ""

        save.setOnClickListener {
            val currentPhoneNumberValue = (layout.findViewById<View>(R.id.current_phone_number) as TextView).text.toString().trim()
            val newPhoneNumberValue = (layout.findViewById<View>(R.id.new_phone_number) as TextView).text.toString().trim()
            val newPhoneNumber = layout.findViewById<View>(R.id.new_phone_number) as TextView

            if (!LoginActivity().isPhoneValid(newPhoneNumberValue)) {
                ViewHelper().showErrorWithRequestFocus(newPhoneNumber, getString(R.string.error_invalid_phone))
            } else if (currentPhoneNumberValue == newPhoneNumberValue) {
                ViewHelper().showErrorWithRequestFocus(newPhoneNumber, getString(R.string.phone_number_must_not))
            } else {
                ToastUtils.ToastShort(ctx, "success")

                //TODO Добавить рест метод для смены номера
            }
        }

        cancel.setOnClickListener {
            dismiss()
        }

        //отключение видимости заднего  layout
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // alert.setCancelable(false); <-- dont' use that instead use bellow approach
        isCancelable = true// <-  press back button not cancel dialog, this one works fine
        alert.setCanceledOnTouchOutside(false)// <- to cancel outside touch
        return alert
    }
}