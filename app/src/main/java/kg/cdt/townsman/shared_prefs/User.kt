package kg.cdt.townsman.shared_prefs

import kg.cdt.townsman.utils.JSONConvertable
import kg.cdt.townsman.wsapi.model.TownsmanUsers

data class User(
    var login: String = "",
    var password: String = "",
    var user_info: TownsmanUsers? = null
) : JSONConvertable
