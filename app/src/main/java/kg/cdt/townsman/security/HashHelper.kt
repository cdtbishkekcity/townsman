package kg.cdt.townsman.security

import com.blankj.utilcode.util.EncryptUtils

object HashHelper {
    fun genPassword2SHA256(password: String) = EncryptUtils.encryptSHA256ToString(password) ?: ""//.substring(9, 33)
}