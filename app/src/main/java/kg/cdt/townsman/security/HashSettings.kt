package kg.cdt.townsman.security

object HashSettings {
    private const val hashSalt = "U6xtUtT9%:yH`K7v|`Og[uuZI"

    fun getHashSalt(): String = hashSalt
}