package kg.cdt.townsman.security

import kg.cdt.townsman.wsapi.model.UprMobileAppSettings
import kg.cdt.townsman.utils.GsonUtils
import kg.cdt.townsman.utils.StorageUtils
import java.util.*

class BlockUserHelper {
    private val TAG = this.javaClass.name
    private val gsonUtils = GsonUtils()

    private var quantityIncorrectInput: Int = 5                              //Кол-во неправильного ввода
    private var resetQuantityIncorrectInputAfterMinutes: Int = 180 / 60      //Сброс количесво попыток в течении 5 минут если поль-ль не заблокирован
    private val timer = Timer()
    private var timerDelay = 5L * 1000
    private var timerPeriod = 30L * 1000

    private fun getSettingsBlockUser() {
        val userAppSettings = gsonUtils.fromJson(StorageUtils().getUserAppSettings(), UprMobileAppSettings::class.java)

        quantityIncorrectInput = userAppSettings.block_pass_failed_count.toInt()
        resetQuantityIncorrectInputAfterMinutes = userAppSettings.block_pass_failed_sec.toInt() / 60
    }

//    fun blockUserLogin(context: Context, editText: Boolean, textView: TextView): Boolean {
//        getSettingsBlockUser()
//        try {
//            val guardianPrefs = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//            if (guardianPrefs.quantity_incorrect_password >= quantityIncorrectInput) {
//                //Установка времени если = exampleTime или invalid_access_key_entry_time == ""
//                if (guardianPrefs.invalid_access_key_entry_time.isEmpty() || guardianPrefs.invalid_access_key_entry_time.isEmpty()) {
//                    guardianPrefs.invalid_access_key_entry_time = TimeUtils.getNowString()
//                }
//                if (!guardianPrefs.blockUser) {
//                    guardianPrefs.count_block_user = ++guardianPrefs.count_block_user
//                    StorageUtils().saveBlockUser(gsonUtils.toJson(guardianPrefs))
//                }
//                guardianPrefs.blockUser = true
//
//                if (editText) {
//                    textView.error = context.getString(
//                        R.string.error_incorrect_access_key_user_block,
//                        quantityIncorrectInput,
//                        numberMinutesToBlockUser()
//                    )
//                    textView.requestFocus()
//                } else if (!editText) {
//                    textView.text = context.getString(
//                        R.string.error_incorrect_access_key_user_block,
//                        quantityIncorrectInput,
//                        numberMinutesToBlockUser()
//                    )
//                }
//
//                LogUtils.error(TAG, "numberMinutesToBlockUser " + numberMinutesToBlockUser())
//
//                StorageUtils().saveBlockUser(gsonUtils.toJson(guardianPrefs))
//                return true
//            }
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//        return false
//    }
//
//    fun unblockUserAfterMinutes() {
//        getSettingsBlockUser()
//        try {
//            timer.scheduleAtFixedRate(object : TimerTask() {
//                override fun run() {
//
//                    val getPrefsGuardian = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//                    if (getPrefsGuardian.invalid_access_key_entry_time != "" && getPrefsGuardian.blockUser) {
//                        val dateTime = DateUtils.addMinutesInTimePlusMinutes(
//                            DateUtils.mySqlFormat,
//                            getPrefsGuardian.invalid_access_key_entry_time,
//                            numberMinutesToBlockUser()
//                        )
//
//                        if (TimeUtils.getNowString() >= dateTime && getPrefsGuardian.quantity_incorrect_password >= quantityIncorrectInput) {
//                            resetAmountInNotCorrectInputIfSuccess()
//                        }
//                    }
//                }
//            }, timerDelay, timerPeriod)
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//    }
//
//    fun resetQuantityIncorrectInputAfterMinutes() {
//        getSettingsBlockUser()
//        try {
//            timer.scheduleAtFixedRate(object : TimerTask() {
//                override fun run() {
//                    val getPrefsGuardian = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//                    if (getPrefsGuardian.invalid_access_key_entry_time != "") {
//                        val dateTime = DateUtils.addMinutesInTimePlusMinutes(
//                            DateUtils.mySqlFormat,
//                            getPrefsGuardian.invalid_access_key_entry_time,
//                            resetQuantityIncorrectInputAfterMinutes
//                        )
//                        if (TimeUtils.getNowString() >= dateTime && getPrefsGuardian.quantity_incorrect_password < quantityIncorrectInput) {
//                            resetAmountInNotCorrectInputIfSuccess()
//                        }
//                    }
//                }
//            }, timerDelay, timerPeriod)
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//    }
//
//    fun failedLoginTryCountAutoIncrement() {
//        getSettingsBlockUser()
//        try {
//            val getPrefsGuardian = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//            if (getPrefsGuardian.invalid_access_key_entry_time.isEmpty() || getPrefsGuardian.invalid_access_key_entry_time.isEmpty()) {
//                getPrefsGuardian.invalid_access_key_entry_time = TimeUtils.getNowString()
//            }
//            getPrefsGuardian.quantity_incorrect_password =
//                ++getPrefsGuardian.quantity_incorrect_password
//
//            StorageUtils().saveBlockUser(gsonUtils.toJson(getPrefsGuardian))
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//    }
//
//    fun resetAmountInNotCorrectInputIfSuccess() {
//        getSettingsBlockUser()
//        try {
//            val getPrefsGuardian = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//            getPrefsGuardian.quantity_incorrect_password = 0
//            getPrefsGuardian.blockUser = false
//            getPrefsGuardian.invalid_access_key_entry_time = ""
//            StorageUtils().saveBlockUser(gsonUtils.toJson(getPrefsGuardian))
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//    }
//
//    fun resetCountBlockUserIfSuccess() {
//        getSettingsBlockUser()
////        val getPrefsGuardian = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//        try {
////            getPrefsGuardian.count_block_user = 0
////            StorageUtils().saveBlockUser(gsonUtils.toJson(getPrefsGuardian))
//            StorageUtils().removeBlockUser()
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//    }
//
//    private fun numberMinutesToBlockUser(): Int {
//        val getPrefsGuardian = StorageUtils().getBlockUser().toObject() ?: BlockUserModel()
//        try {
//            val firstBlockMinutes = 1
//            val secondBlockMinutes = 2
//            val thirdBlockMinutes = 3
//            val fourthBlockMinutes = 4
//            val fifthBlockMinutes = 5
//
//            when {
//                getPrefsGuardian.count_block_user == 1 -> {
//                    return firstBlockMinutes //Количество минут
//                }
//                getPrefsGuardian.count_block_user == 2 -> {
//                    return secondBlockMinutes
//                }
//                getPrefsGuardian.count_block_user == 3 -> {
//                    return thirdBlockMinutes
//                }
//                getPrefsGuardian.count_block_user == 4 -> {
//                    return fourthBlockMinutes
//                }
//                getPrefsGuardian.count_block_user >= 5 -> {
//                    return fifthBlockMinutes
//                }
//            }
//        } catch (e: Exception) {
//            LogUtils.error(TAG, e.message, e)
//        }
//        return 0
//    }
}