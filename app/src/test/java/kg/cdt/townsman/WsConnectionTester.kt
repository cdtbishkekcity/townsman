package kg.cdt.townsman

import com.blankj.utilcode.util.EncryptUtils
import kg.cdt.townsman.security.HashSettings
import kg.cdt.townsman.utils.DateTimeUtils
import kg.cdt.townsman.utils.LogUtils
import kg.cdt.townsman.utils.OkHttpUtils
import kg.cdt.townsman.utils.ThreadGuiUtils.doInThreadPool
import kg.cdt.townsman.wsapi.WsEndpoints
import kg.cdt.townsman.wsapi.WsRestConnector
import org.junit.Test

class WsConnectionTester {
    private val TAG = this.javaClass.name

    //    private val endpoint = "https://upr.meria.kg/townsman-ws/"
    private val endpoint = "http://${WsEndpoints.wsHubDomainDev}/townsman-ws/"

    init {
        OkHttpUtils.LogError = false
        WsRestConnector.wsURL = endpoint
        LogUtils.TEST_MODE = true
    }

    @Test
    fun wsRestLoginLogoutTester() {
        doInThreadPool {
            try {
                println("${DateTimeUtils().nowMs()} start ws rest connector test")

                val passwordHash =
                    EncryptUtils.encryptHmacSHA384ToString("123456", HashSettings.getHashSalt())
                println(passwordHash)

                // try login
                val tryLogin =
                    WsRestConnector.getInstance().tryLogin("996555111111", passwordHash)//PIN,PASSWORD
                if (tryLogin) {
                    println("${DateTimeUtils().nowMs()} login success")
                    println(WsRestConnector.getInstance().SessionHeader)


                    // -------------------------------
/*                    if (false) {
                        println("${DateTimeUtils().nowMs()} getUserPersonal()")
                        val getUserPersonal =
                            WsRestConnector.getInstance().getUserPersonal() ?: BaseResponse.Result()
                        if (getUserPersonal.user_info?.pin?.isNotEmpty() == true) {
                            println(getUserPersonal)
                        }
                    }

                    // -------------------------------
                    if (false) {
                        Thread.sleep(1000L)
                        println("${DateTimeUtils().nowMs()} setRegisteredPushToken")

                        val setPushToken = WsRestConnector.getInstance().setRegisteredPushToken(
                            "7g6XXS3UVQur2FnEAWnuHatBqEbliuipCDF2LWfW2QfPggSTDo0k1mvd6MlLlCR8fOEVwXm3QoAUMbUwq2VROSEGlrwAUcsKozoiFkzeutjzPS3joaJkGIO22bSRg1hpSDM6dOKSoxGUjYyrZd",
                            "0.1.2.3",
                            "Test os version"
                        ) ?: TownsmanUsers()
                        if (setPushToken.pin != null) {
                            println(setPushToken.toString())
                        }
                    }

                    // -------------------------------
                    if (false) {
                        Thread.sleep(1000L)
                        println("${DateTimeUtils().nowMs()} setUserPersonal")

                        val setUserInfo = WsRestConnector.getInstance().setUserPersonal(
                            "asd@mail.ru",
                            "Фыва111",
                            ""
                        ) ?: TownsmanUsers()
                        if (setUserInfo.pin != null) {
                            println(setUserInfo.toString())
                        }
                    }*/
                    // try logout
                    tryLogOut()
                } else {
                    println(tryLogin)
                    println("${DateTimeUtils().nowMs()} login failed")
                }
            } catch (e: Exception) {
                println("$TAG: ${e.message}")

                // try logout
                tryLogOut()
            }
        }.get()
    }

    private fun tryLogOut() {
        // try logout
        val tryLogout = WsRestConnector.getInstance().tryLogout()
        println(WsRestConnector.getInstance().SessionHeader)

        if (tryLogout) {
            println("${DateTimeUtils().nowMs()} logout success")
        } else {
            println("${DateTimeUtils().nowMs()} logout failed")
        }
    }

/*    @Test
    fun tryGetAppSettingsWithoutLogin() {
        doInThreadPool {
            try {
                println("${DateTimeUtils().nowMs()} getMobileAppSettings()")
                val getMobileAppSettings = WsRestConnector.getInstance().getMobileAppSettings() ?: UprMobileAppSettings()
                println(getMobileAppSettings)
            } catch (e: Exception) {
                println(e)
            }
        }.get()
    }

    @Test
    fun endpointPerformanceTester() {
        val results = HashMap<Int, Future<*>?>()
        for (id in 1..30) {
            results[id] = doInThreadPool {
                try {
//                    val response = OkHttpUtils()
//                        .tryGet(
//                            "",//URL
//                            3,
//                            7000L
//                        )

                    val response = WsRestConnector.getInstance().getMobileAppSettings()
                        ?: UprMobileAppSettings()

                    println("${DateTimeUtils().nowMs()}: $response")
                } catch (e: Exception) {
                    println(e)
                }
            }

            Thread.sleep(10L)
        }

        if (results.isNotEmpty()) {
            for (result in results) {
                val key = result.key
                val value = result.value?.get()
            }
        }
    }*/

}