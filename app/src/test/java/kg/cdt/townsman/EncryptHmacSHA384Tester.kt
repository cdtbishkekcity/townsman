package kg.cdt.townsman

import com.blankj.utilcode.util.EncryptUtils
import kg.cdt.townsman.security.HashSettings
import kg.cdt.townsman.utils.DateTimeUtils
import org.junit.Assert
import org.junit.Test

class EncryptHmacSHA384Tester {
    private val password = "asdf"

    @Test
    fun run1() {
        //1
        val now = DateTimeUtils().nowMs()
        var encryptHmacSHA384string = EncryptUtils.encryptHmacSHA384ToString(password, now)
        println(encryptHmacSHA384string)

        //2
        val time = "2019-11-26 16:23:51.333"
        encryptHmacSHA384string = EncryptUtils.encryptHmacSHA384ToString(password, time)
        println(encryptHmacSHA384string)

        Assert.assertEquals(encryptHmacSHA384string, "2989190B9028815A20B825C15EBB1B1AA0DE7CAF508DF710A5E2FD0E4248014944C2BB48F8449E0618B0815F03BB36D5")

        //3
        encryptHmacSHA384string = EncryptUtils.encryptHmacSHA384ToString(password, HashSettings.getHashSalt())
        println(encryptHmacSHA384string)

        Assert.assertEquals(encryptHmacSHA384string, "560CB8DAAB6DB3B61EF26641BF840591996EBE4C7289962240E2F479A05E91B2CC4F1CB129F014A4F8CEBA0A470CD66D")
    }
}